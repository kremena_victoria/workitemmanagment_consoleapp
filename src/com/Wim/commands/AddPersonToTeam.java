package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.Wim.commands.CommandConstants.*;

public class AddPersonToTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;
    private static final String PERSON_ACTIVITY_MESSAGE = "%s added %s as a Member to Team %s, %s";

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public AddPersonToTeam(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personToAddNewMemberToTeam = parameters.get(0);
        String teamToAddToName = parameters.get(1);
        String personToBeAddedName = parameters.get(2);

        CommandValidationHelper.checkPersonNotCreated(wimRepository, personToAddNewMemberToTeam);
        CommandValidationHelper.checkTeamNotCreated(wimRepository, teamToAddToName);
        CommandValidationHelper.checkPersonNotCreated(wimRepository, personToBeAddedName);

        ITeam team = wimRepository.getTeams().get(teamToAddToName);
        IPerson personAddingNewPersonToTeam = wimRepository.getPersons().get(personToAddNewMemberToTeam);

        CommandValidationHelper.checkPersonNotMemberInTeam(team,personAddingNewPersonToTeam);

        IPerson personToBeAddedToTeam = wimRepository.getPersons().get(personToBeAddedName);
        if (team.getTeamMembers().contains(personToBeAddedToTeam)) {
            return PERSON_EXISTS_ERROR_MESSAGE;
        }
        team.addPersonToTeam(personToBeAddedToTeam);

        personAddingNewPersonToTeam.addActivityHistory(String.format(PERSON_ACTIVITY_MESSAGE,
                personToAddNewMemberToTeam,
                personToBeAddedName,
                team.getTeamName(),
                LOCAL_TIME_NOW));
        team.addActivityHistory(String.format("%s person was added to Team",personToBeAddedName));

        return String.format(PERSON_ADDED_TO_TEAM_SUCCESS_MESSAGE, personToBeAddedName, teamToAddToName);
    }
}
