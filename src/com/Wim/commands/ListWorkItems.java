package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.enums.*;
import com.Wim.models.workItemContracts.IWorkItem;

import java.util.List;

public class ListWorkItems implements Command {
        private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

        private WimRepository wimRepository;
        private WimFactory wimFactory;

        public ListWorkItems(WimRepository wimRepository, WimFactory wimFactory) {
            this.wimRepository = wimRepository;
            this.wimFactory = wimFactory;
        }

        @Override
        public String execute(List<String> parameters) {
            CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
            String teamName = parameters.get(0);
            String boardName = parameters.get(1);
            String instruction = parameters.get(2).replaceAll("\"","");
            int rating = 0;
            String filterComparator = "";
            if(parameters.get(3).chars().allMatch(Character::isDigit)){
                rating = Integer.parseInt(parameters.get(3));
            }else{
                filterComparator = parameters.get(3).replaceAll("\"","");
            }
            String assignee = parameters.get(4);

            CommandValidationHelper.checkTeamNotCreated(wimRepository,teamName);
            CommandValidationHelper.checkBoardNotCreated(wimRepository,boardName);
            
            ITeam team = wimRepository.getTeams().get(teamName);
            IBoard board = wimRepository.getBoards().get(boardName);
            CommandValidationHelper.checkBoardNotBelongsToTeam(team, board);
            IPerson person = wimRepository.getPersons().get(assignee);

            String result = "";
            switch (instruction.toLowerCase()) {
                case "list all":
                    result = board.listAll();
                    break;
                case "filter bugs":
                    result = board.filterWorkItemsByType("Bug");
                    break;
                case "filter stories":
                    result = board.filterWorkItemsByType("Story");
                    break;
                case "filter feedback":
                    result = board.filterWorkItemsByType("Feedback");
                    break;
                case "filter title":
                    result = board.filterWorkItemsByTitle(filterComparator);
                    break;
                case "filter rating":
                    result = board.filterWorkItemsByRating(rating);
                    break;
                case "filter assignee":
                    CommandValidationHelper.checkPersonNotCreated(wimRepository,assignee);
                    CommandValidationHelper.checkPersonNotMemberInTeam(team,person);
                    result = board.filterWorkItemsByAssignee(assignee);
                    break;
                case "filter priority":
                    if((Priority.valueOf(filterComparator.toUpperCase()).name()).equals(filterComparator.toUpperCase()))
                    result = board.filterWorkItemsByPriority(filterComparator);
                    break;
               case "filter status and assignee":
                       result = board.filterWorkItemsByStatusAndAssignee(filterComparator, assignee);
                    break;
                case "filter severity":
                        result = board.filterWorkItemsBySeverity(filterComparator);
                    break;
                case "filter size":
                        result = board.filterWorkItemsBySize(filterComparator);
                    break;
                case "filter status":
                        result = board.filterWorkItemsByStatus(filterComparator);
                    break;
            }
            return result;
        }
}
