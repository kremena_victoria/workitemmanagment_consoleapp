package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.workItemContracts.Comment;
import com.Wim.models.workItemContracts.IWorkItem;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.Wim.commands.CommandConstants.*;

public class AddCommentsToAWorkItem implements Command {

    private static final int EXPECTED_PARAMETERS = 3;
    private static final String COMMENT_CREATED_SUCCESS_MESSAGE = "%s sucessfully created";

    private WimRepository wimRepository;
    private WimFactory wimFactory;

    public AddCommentsToAWorkItem(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != EXPECTED_PARAMETERS) {
            throw new IllegalArgumentException();
        }

        String personCreatingComment = parameters.get(0);
        Integer workItemForCommentId = Integer.parseInt(parameters.get(1));
        String comment = parameters.get(2);
        CommandValidationHelper.checkPersonNotCreated(wimRepository,personCreatingComment);
        return addCommentsToAWorkItem(personCreatingComment, workItemForCommentId, comment);
    }

    private String addCommentsToAWorkItem(String personName, Integer workItemId, String realComment) {
        IPerson person = wimRepository.getPersons().get(personName);
        IWorkItem workItemID = wimRepository.getWorkItems().get(workItemId);
        Comment comment = wimFactory.createCommentToWorkItem(personName, workItemID, realComment);
        comment.setAuthor(personName);
        comment.setMessage(realComment);
        String commentContent = comment.getComment();
        person.addActivityHistory(String.format("%s added comment to work item Id. %d, type: %s, %s",
                personName,
                workItemId,
                workItemID.getType(),
                LOCAL_TIME_NOW));
        return String.format(COMMENT_CREATED_SUCCESS_MESSAGE, commentContent);
    }
}
