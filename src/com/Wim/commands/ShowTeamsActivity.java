package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.ITeam;

import java.util.List;

public class ShowTeamsActivity implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ShowTeamsActivity(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String teamName = parameters.get(0);
        CommandValidationHelper.checkTeamNotCreated(wimRepository, teamName);
        ITeam team = wimRepository.getTeams().get(teamName);
        //StringBuilder sb = new StringBuilder();

        return team.showTeamsActivity();
    }
}
