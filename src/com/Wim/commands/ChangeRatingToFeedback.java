package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.workItemContracts.IFeedback;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.LOCAL_TIME_NOW;
import static com.Wim.commands.CommandConstants.RATING_CHANGED_SUCCESS_MESSAGE;

public class ChangeRatingToFeedback implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;
    private static final String PERSON_ACTIVITY_MESSAGE = "%s changed rating to work item Feedback No. %d to %d, %s";

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ChangeRatingToFeedback(WimRepository wimRepository, WimFactory wimFactory) {
       this.wimRepository = wimRepository;
       this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personToChangeRating = parameters.get(0);
        String teamToChangeRating = parameters.get(1);
        String boardNameToSetRatingSize = parameters.get(2);
        int workItemNoToSetRating = Integer.parseInt(parameters.get(3));
        int setRatingSize = Integer.parseInt(parameters.get(4));

        CommandValidationHelper.checkPersonNotCreated(wimRepository, personToChangeRating);
        CommandValidationHelper.checkTeamNotCreated(wimRepository, teamToChangeRating);
        CommandValidationHelper.checkBoardNotCreated(wimRepository, boardNameToSetRatingSize);
        return workItemToSetRating(personToChangeRating,teamToChangeRating,workItemNoToSetRating, boardNameToSetRatingSize, setRatingSize);
    }

    private String workItemToSetRating(String person, String team, int id, String boardName, int rating) {
        ITeam teamToChangeR = wimRepository.getTeams().get(team);
        IPerson personChangeRate = wimRepository.getPersons().get(person);
        CommandValidationHelper.checkPersonNotMemberInTeam(teamToChangeR,personChangeRate);

        IBoard board = wimRepository.getBoards().get(boardName);
        CommandValidationHelper.checkWorkItemIdIsCorrect(id,board);
        if(!board.getBoardWorkItems().get(id).getType().equals("Feedback")){
            throw new IllegalArgumentException("The entered work item is not a Feedback");
        }
        IFeedback feedback = (IFeedback) board.getBoardWorkItems().get(id);
        feedback.changFeedbackRating(rating);
        feedback.addHistory(String.format("%s rating was changed",feedback.getType()));
        personChangeRate.addActivityHistory(String.format(PERSON_ACTIVITY_MESSAGE,
                person,
                feedback.getId(),
                rating,
                LOCAL_TIME_NOW));
        board.addActivityHistory(String.format("Rating of Feedback No.: %d in board %s was changed",
                feedback.getId(),
                boardName));
        feedback.addHistory(String.format("Rating of FeedBack item No. %d, has been changed to %d",
                feedback.getId(),
                rating));
        return String.format(RATING_CHANGED_SUCCESS_MESSAGE, feedback.getType(), rating);
    }
}
