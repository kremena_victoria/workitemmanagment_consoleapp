package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;

import java.util.List;

public class ShowBoardsActivity implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ShowBoardsActivity(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String boardName = parameters.get(0);
        CommandValidationHelper.checkBoardNotCreated(wimRepository, boardName);
        IBoard board = wimRepository.getBoards().get(boardName);
        return board.showBoardActivity();
    }
}

