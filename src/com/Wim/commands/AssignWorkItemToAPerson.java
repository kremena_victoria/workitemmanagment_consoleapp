package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.workItemContracts.BugStoryBase;
import com.Wim.models.workItemContracts.IWorkItem;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.ASSIGN_WORKITEM_TO_PERSON_SUCCESS_MESSAGE;
import static com.Wim.commands.CommandConstants.LOCAL_TIME_NOW;

public class AssignWorkItemToAPerson implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 4;

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public AssignWorkItemToAPerson(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters, CORRECT_NUMBER_OF_ARGUMENTS);

        String teamInWhichBelongPerson = parameters.get(0);
        String boardInWhichBelongWorkItem = parameters.get(1);
        int workItemId = Integer.parseInt(parameters.get(2));
        String assignPersonName = parameters.get(3);

        CommandValidationHelper.checkTeamNotCreated(wimRepository, teamInWhichBelongPerson);
        CommandValidationHelper.checkBoardNotCreated(wimRepository,boardInWhichBelongWorkItem);

        ITeam team = wimRepository.getTeams().get(teamInWhichBelongPerson);
        IBoard board = wimRepository.getBoards().get(boardInWhichBelongWorkItem);
        CommandValidationHelper.checkBoardNotBelongsToTeam(team,board);
        CommandValidationHelper.checkWorkItemIdIsCorrect(workItemId,board);

        IWorkItem workItem = board.getBoardWorkItems().get(workItemId);
        IPerson person = wimRepository.getPersons().get(assignPersonName);
        CommandValidationHelper.checkPersonNotMemberInTeam(team,person);

        person.assignWorkItemToPerson(workItem);
        String typeWorkItem = workItem.getType();
        if(typeWorkItem.equals("Bug") || typeWorkItem.equals("Story")){
            BugStoryBase bugOrStoryItem = (BugStoryBase) workItem;
            bugOrStoryItem.setAssignee(assignPersonName);
            board.addBugStoryToBoard(bugOrStoryItem);
        }else{
            throw new IllegalArgumentException("Feedback cannot be assigned to a person");
        }
        board.addActivityHistory(String.format("Work item No.: %d, %s in board %s was assigned to %s",
                workItem.getId(),
                workItem.getType(),
                boardInWhichBelongWorkItem,
                assignPersonName));
        person.addActivityHistory(String.format("%s has assigned work item No. %d, type %s, %s",
                assignPersonName,
                workItemId,
                workItem.getType(),
                LOCAL_TIME_NOW));
        workItem.addHistory(String.format("Work item No. %d, type: %s has been assigned to %s",
                workItemId,
                workItem.getType(),
                assignPersonName));

        return String.format(ASSIGN_WORKITEM_TO_PERSON_SUCCESS_MESSAGE, workItemId, assignPersonName)+ "\n" +
                "work Item No: " + workItemId + " " + workItem.getType() + " " +workItem.getTitle() ;
    }
}
