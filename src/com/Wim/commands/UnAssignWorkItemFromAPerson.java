package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.workItemContracts.BugStoryBase;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.common.ValidationHelper;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.LOCAL_TIME_NOW;
import static com.Wim.commands.CommandConstants.UNASSIGN_WORKITEM_TO_PERSON_SUCCESS_MESSAGE;

public class UnAssignWorkItemFromAPerson implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 4;
    private static final String BOARD_NOT_EXISTS_IN_TEAM_ERROR_MESSAGE = "No such board in a team";
    private static final String THIS_PERSON_NOT_EXISTS_IN_TEAM_ERROR_MESSAGE = "No such person in a team";
    private static final String TEAM_NOT_EXISTS_ERROR_MESSAGE = "No such a team";
    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public UnAssignWorkItemFromAPerson(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);

        String teamInWhichBelongPerson = parameters.get(0);
        String boardInWhichBelongWorkItem = parameters.get(1);
        int workItemId = Integer.parseInt(parameters.get(2));
        String unAssignPersonName = parameters.get(3);
        if(!wimRepository.getTeams().containsKey(teamInWhichBelongPerson)){
            throw new IllegalArgumentException(TEAM_NOT_EXISTS_ERROR_MESSAGE);
        }
        ITeam team = wimRepository.getTeams().get(teamInWhichBelongPerson);
        IBoard board = wimRepository.getBoards().get(boardInWhichBelongWorkItem);

        if(!team.getTeamBoards().contains(board)){
            throw new IllegalArgumentException(BOARD_NOT_EXISTS_IN_TEAM_ERROR_MESSAGE);
        }

        if (workItemId < 0 || workItemId > board.getBoardWorkItems().size()) {
            return "No existing id of work item";
        }
        IWorkItem workItem = board.getBoardWorkItems().get(workItemId);
        IPerson person = wimRepository.getPersons().get(unAssignPersonName);
        CommandValidationHelper.checkPersonNotMemberInTeam(team,person);
        person.unAssignWorkItemToPerson(workItem);
        String typeWorkItem = workItem.getType();
        if(typeWorkItem.equals("Bug") || typeWorkItem.equals("Story")){
            BugStoryBase bugOrStoryItem = (BugStoryBase) workItem;
            bugOrStoryItem.setAssignee("no assignee");
            board.removeBugStoryToBoard(bugOrStoryItem);
        }else {
            throw new IllegalArgumentException("Feedback cannot be assigned to a person");
        }
        board.addActivityHistory(String.format("Work item No.: %d, %s in board %s was assigned to %s",
                workItem.getId(),
                workItem.getType(),
                boardInWhichBelongWorkItem,
                unAssignPersonName));
        person.addActivityHistory(String.format("%s has assigned work item No. %d, type %s, %s",
                unAssignPersonName,
                workItemId,
                workItem.getType(),
                LOCAL_TIME_NOW));
        workItem.addHistory(String.format("Work item No. %d, type: %s has been assigned to %s",
                workItemId,
                workItem.getType(),
                unAssignPersonName));
        return String.format(UNASSIGN_WORKITEM_TO_PERSON_SUCCESS_MESSAGE, workItemId, unAssignPersonName);
    }

}

