package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IPerson;
import java.util.List;
import static com.Wim.commands.CommandConstants.PERSON_CREATED_SUCCESS_MESSAGE;

public class CreateNewPerson implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private WimRepository wimRepository;
    private WimFactory wimFactory;

    public CreateNewPerson(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personName = parameters.get(0);
        return createNewPerson(personName);
    }
    private String createNewPerson(String personName) {
        CommandValidationHelper.checkPersonAlreadyExists(wimRepository,personName);

        IPerson person = wimFactory.createPerson(personName);
        wimRepository.addPerson(personName,person);
        return String.format(PERSON_CREATED_SUCCESS_MESSAGE, personName);
    }
}
