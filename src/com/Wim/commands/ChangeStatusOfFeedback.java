package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.enums.FeedbackStatus;
import com.Wim.models.workItemContracts.IFeedback;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.LOCAL_TIME_NOW;
import static com.Wim.commands.CommandConstants.STATUS_CHANGED_SUCCESS_MESSAGE;

public class ChangeStatusOfFeedback implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;
    private static final String PERSON_ACTIVITY_MESSAGE = "%s changed status of work item Feedback No. %d to %s, %s";

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ChangeStatusOfFeedback (WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personToChangeStatus = parameters.get(0);
        String teamToChangeStatus = parameters.get(1);
        String boardNameToSetStatus = parameters.get(2);
        int workItemIdToSetStatus = Integer.parseInt(parameters.get(3));
        String changedStatus = parameters.get(4);

        CommandValidationHelper.checkPersonNotCreated(wimRepository, personToChangeStatus);
        CommandValidationHelper.checkTeamNotCreated(wimRepository, teamToChangeStatus);
        CommandValidationHelper.checkBoardNotCreated(wimRepository, boardNameToSetStatus);
        return workItemToSetStatus(personToChangeStatus,teamToChangeStatus,boardNameToSetStatus, workItemIdToSetStatus, changedStatus);
    }

    private String workItemToSetStatus(String person, String team, String boardName,int id, String status) {
        ITeam teamToChangeStatus = wimRepository.getTeams().get(team);
        IPerson personChangeRate = wimRepository.getPersons().get(person);
        CommandValidationHelper.checkPersonNotMemberInTeam(teamToChangeStatus,personChangeRate);

        IBoard board = wimRepository.getBoards().get(boardName);
        CommandValidationHelper.checkWorkItemIdIsCorrect(id,board);
        if(!board.getBoardWorkItems().get(id).getType().equals("Feedback")){
            throw new IllegalArgumentException("The entered work item is not a Feedback");
        }
        IFeedback feedback = (IFeedback) board.getBoardWorkItems().get(id);
        feedback.changeFeedbackStatus(FeedbackStatus.valueOf(status.toUpperCase()));
        feedback.addHistory(String.format("%s status was changed",feedback.getType()));
        personChangeRate.addActivityHistory(String.format(PERSON_ACTIVITY_MESSAGE,
                person,
                feedback.getId(),
                status,
                LOCAL_TIME_NOW));
        board.addActivityHistory(String.format("Status of Feedback No.: %d in board %s was changed to %s",
                feedback.getId(),
                boardName,
                status));
        feedback.addHistory(String.format("Status of FeedBack item No. %d, has been changed to %s",
                feedback.getId(),
                status));

        return String.format(STATUS_CHANGED_SUCCESS_MESSAGE, feedback.getType(), status);
    }
}
