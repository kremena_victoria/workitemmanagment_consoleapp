package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IPerson;

import java.util.List;

public class ShowPersonActivity implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ShowPersonActivity(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personName = parameters.get(0);
        CommandValidationHelper.checkPersonNotCreated(wimRepository, personName);
        IPerson person = wimRepository.getPersons().get(personName);
        return person.showActivity();
    }
}
