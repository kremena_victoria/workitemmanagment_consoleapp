package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;

import java.util.List;

import static com.Wim.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllTeams implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 0;
    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ShowAllTeams(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        if(wimRepository.getTeams().isEmpty()){
            return "No Team created";
        }
        return "Team are: " + wimRepository.getTeams().keySet().toString()
                .replaceAll("\\[", "")
                .replaceAll("\\]", "");
    }
}
