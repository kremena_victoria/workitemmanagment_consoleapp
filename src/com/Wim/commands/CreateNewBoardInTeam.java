package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.*;

public class CreateNewBoardInTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public CreateNewBoardInTeam(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personAddingBoard = parameters.get(0);
        String teamNameToCreateBoard = parameters.get(1);
        String boardNameToBeCreated = parameters.get(2);
        CommandValidationHelper.checkPersonNotCreated(wimRepository,personAddingBoard);
        CommandValidationHelper.checkTeamNotCreated(wimRepository,teamNameToCreateBoard);
        return createNewBoardInTeam(personAddingBoard, boardNameToBeCreated, teamNameToCreateBoard);
    }

    private String createNewBoardInTeam(String person,String boardName, String teamName) {
        ITeam team = wimRepository.getTeams().get(teamName);
        IPerson personAddingNewBoard = wimRepository.getPersons().get(person);
        CommandValidationHelper.checkPersonNotMemberInTeam(team,personAddingNewBoard);
        if (wimRepository.getBoards().containsKey(boardName)) {
            return String.format(BOARD_EXISTS_ERROR_MESSAGE, boardName);
        }

        IBoard board = wimFactory.createBoard(boardName);
        wimRepository.addBoard(boardName,board);

        IBoard boardToBeAddedInTeam = wimRepository.getBoards().get(boardName);
        if (!wimRepository.getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_FOUND_ERROR_MESSAGE, boardToBeAddedInTeam));
        }
        team.addBoardToTeam(boardToBeAddedInTeam);
        personAddingNewBoard.addActivityHistory(String.format("%s created new board %s in team %s, %s",
                person,
                boardName,
                team.getTeamName(),
                LOCAL_TIME_NOW));
        team.addActivityHistory(String.format("%s board was added to Team",boardName));

        return String.format(BOARD_CREATED_SUCCESS_MESSAGE, boardName,teamName);
    }

}
