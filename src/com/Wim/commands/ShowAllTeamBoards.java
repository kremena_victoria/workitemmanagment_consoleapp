package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;

import java.util.List;

import static java.lang.String.format;

public class ShowAllTeamBoards implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowAllTeamBoards(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String teamToShowAllBoards = parameters.get(0);
        return showAllTeamBoards(teamToShowAllBoards);
    }

    private String showAllTeamBoards(String teamName) {
        CommandValidationHelper.checkTeamNotCreated(wimRepository,teamName);
        return wimRepository.getTeams().get(teamName).boardList();
    }
}
