package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import java.util.List;

import static com.Wim.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllPeople implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 0;
    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ShowAllPeople(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        if(wimRepository.getPersons().isEmpty()){
            return "No Person created";
        }
        return "People are: " + wimRepository.getPersons().keySet().toString()
                .replaceAll("\\[", "")
                .replaceAll("\\]", "");
    }
}
