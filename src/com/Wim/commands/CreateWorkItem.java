package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.workItemContracts.IWorkItem;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.*;

public class CreateWorkItem implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 6;
    private static final String PERSON_ACTIVITY_MESSAGE = "%s added work item %s in board %s, %s";

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public CreateWorkItem(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);

        String personToCreateWorkItem = parameters.get(0);
        String teamNameToCreateWorkItem = parameters.get(1);
        String boardNameToCreateWorkItem = parameters.get(2);
        String workItemTitle = parameters.get(3);
        String workItemDescription = parameters.get(4);
        String type = parameters.get(5);

        CommandValidationHelper.checkPersonNotCreated(wimRepository, personToCreateWorkItem);
        CommandValidationHelper.checkTeamNotCreated(wimRepository,teamNameToCreateWorkItem);
        CommandValidationHelper.checkBoardNotCreated(wimRepository,boardNameToCreateWorkItem);

        return createNewBugStoryFeedbackInBoard(personToCreateWorkItem, type, workItemTitle, workItemDescription, teamNameToCreateWorkItem, boardNameToCreateWorkItem);
   }
    private String createNewBugStoryFeedbackInBoard(String person, String type, String workItemTitle,
                                                    String workItemDescription,
                                                    String teamName, String boardName) {
        IPerson personToAddWorkItem = wimRepository.getPersons().get(person);
        ITeam team = wimRepository.getTeams().get(teamName);
        CommandValidationHelper.checkPersonNotMemberInTeam(team,personToAddWorkItem);

        IWorkItem workItem = wimFactory.createWorkItem(type, workItemTitle, workItemDescription);
       wimRepository.addWorkItem(workItem.getId(),workItem);
        IBoard board = wimRepository.getBoards().get(boardName);
        CommandValidationHelper.checkBoardNotBelongsToTeam(team,board);
        board.addWorkItemToBoard(workItem);
        if(type.equalsIgnoreCase("Bug") | type.equalsIgnoreCase("Story")){
            board.addBugStoryToBoard(workItem);
        }
        String workItemCreated = String.format("Work item type: %s, Id No.: %d, Title: %s was created.",
                type,
                workItem.getId(),
                workItemTitle);
        workItem.addHistory(workItemCreated);
        personToAddWorkItem.addActivityHistory(String.format(PERSON_ACTIVITY_MESSAGE,
                person,
                workItem.getType(),
                board.getBoardName(),
                LOCAL_TIME_NOW));

        return String.format(WORKITEM_CREATED_SUCCESS_MESSAGE, type, boardName);
    }
}
