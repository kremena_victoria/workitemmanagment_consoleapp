package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.workItemContracts.IBug;


import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.LOCAL_TIME_NOW;

public class AddStepsToReproduceToBug implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;
    private static final String PERSON_ACTIVITY_MESSAGE = "%s added %s to work item Bug No. %d, %s";
    private static final String BUG_SUCCESSFUL_MESSAGE = "Bug steps to Reproduce to Bug was added";

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public AddStepsToReproduceToBug(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters, CORRECT_NUMBER_OF_ARGUMENTS);
        String personToAddBugSteps = parameters.get(0);
        String teamToAddBugSteps = parameters.get(1);
        String boardNameToAddStepsToReproduce = parameters.get(2);
        int bugIdToAddStepsToReproduce = Integer.parseInt(parameters.get(3));
        String stepToReproduceToBeAdded = parameters.get(4);

        CommandValidationHelper.checkPersonNotCreated(wimRepository, personToAddBugSteps);
        CommandValidationHelper.checkTeamNotCreated(wimRepository, teamToAddBugSteps);
        CommandValidationHelper.checkBoardNotCreated(wimRepository, boardNameToAddStepsToReproduce);

        ITeam team = wimRepository.getTeams().get(teamToAddBugSteps);
        IPerson personAddingBugSteps = wimRepository.getPersons().get(personToAddBugSteps);
        CommandValidationHelper.checkPersonNotMemberInTeam(team,personAddingBugSteps);

        IBoard board = wimRepository.getBoards().get(boardNameToAddStepsToReproduce);
        CommandValidationHelper.checkWorkItemIdIsCorrect(bugIdToAddStepsToReproduce,board);

        if(!board.getBoardWorkItems().get(bugIdToAddStepsToReproduce).getType().equals("Bug")){
            throw new IllegalArgumentException("The entered work item is not a Bug");
        }

        IBug bug = (IBug) board.getBoardWorkItems().get(bugIdToAddStepsToReproduce);
        bug.addBugRepreduceSteps(stepToReproduceToBeAdded);
        bug.addHistory(String.format("%s reproduce steps was added",bug.getType()));
        personAddingBugSteps.addActivityHistory(String.format(PERSON_ACTIVITY_MESSAGE,
                personToAddBugSteps,
                stepToReproduceToBeAdded,
                bug.getId(),
                LOCAL_TIME_NOW));
        board.addActivityHistory(String.format("Steps to reproduce to Bug No.: %d in board %s were added",
                bug.getId(),
                boardNameToAddStepsToReproduce));

        return BUG_SUCCESSFUL_MESSAGE;
    }
}
