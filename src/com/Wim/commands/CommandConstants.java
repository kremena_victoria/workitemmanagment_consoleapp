package com.Wim.commands;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class CommandConstants {
    static final String LOCAL_TIME_NOW = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy, HH:mm"));
    static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments.";
    static final String TEAM_NOT_FOUND_ERROR_MESSAGE = "Team %s not found.";
    static final String PERSON_NOT_FOUND_ERROR_MESSAGE = "Person %s not found.";
    static final String PERSON_EXISTS_ERROR_MESSAGE = "Person already Exists";
    static final String TEAM_EXISTS_ERROR_MESSAGE = "Team already Exists";
    static final String BOARD_NOT_FOUND_ERROR_MESSAGE = "Board %s not found";
    static final String BOARD_EXISTS_ERROR_MESSAGE = "Board %s already exists";
    static final String BOARD_NOT_EXISTS_IN_TEAM_ERROR_MESSAGE = "No such board in a team";
    static final String ID_ERROR_MESSAGE = "No existing id of work item";
    static final String PERSON_NOT_EXISTS_IN_TEAM_ERROR_MESSAGE = "No such person in a team";

    static final String PERSON_CREATED_SUCCESS_MESSAGE = "Person %s is created";
    static final String TEAM_CREATED_SUCCESS_MESSAGE = "Team %s is created";
    static final String PERSON_ADDED_TO_TEAM_SUCCESS_MESSAGE = "Person %s added successfully to team %s";
    static final String BOARD_CREATED_SUCCESS_MESSAGE = "Board %s in Team %s created";
    static final String WORKITEM_CREATED_SUCCESS_MESSAGE = "Work item %s in Board %s created.";
    static final String RATING_CHANGED_SUCCESS_MESSAGE = "Work item's %s rating has been changed to %d.";
    static final String STATUS_CHANGED_SUCCESS_MESSAGE = "Work item's %s status has been changed to %s.";
    static final String ASSIGN_WORKITEM_TO_PERSON_SUCCESS_MESSAGE = "Work item %d is assigned to %s.";
    static final String UNASSIGN_WORKITEM_TO_PERSON_SUCCESS_MESSAGE = "Work item %d is unassigned from %s.";
    static final String COMMENT_CREATED_SUCCESS_MESSAGE = "%s sucessfully created";
   }
