package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.enums.*;
import com.Wim.models.workItemContracts.IStory;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.LOCAL_TIME_NOW;

public class ChangePrioritySizeStatusOfStory implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 6;
    private static final String PERSON_ACTIVITY_MESSAGE = "%s changed %s of work item Feedback No. %d to %s, %s";

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ChangePrioritySizeStatusOfStory (WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personToChangeStatus = parameters.get(0);
        String teamToChangeStatus = parameters.get(1);
        String boardNameToSetStatus = parameters.get(2);
        int workItemIdToSetStatus = Integer.parseInt(parameters.get(3));
        String instruction = parameters.get(4);
        String newValue = parameters.get(5);

        CommandValidationHelper.checkPersonNotCreated(wimRepository, personToChangeStatus);
        CommandValidationHelper.checkTeamNotCreated(wimRepository, teamToChangeStatus);
        CommandValidationHelper.checkBoardNotCreated(wimRepository, boardNameToSetStatus);
        return workItemToSetStatus(personToChangeStatus,
                teamToChangeStatus,
                boardNameToSetStatus,
                workItemIdToSetStatus,
                instruction,
                newValue);
    }

    private String workItemToSetStatus(String person, String team, String boardName,int id, String setInstructions, String value) {
        ITeam teamToChangeStatus = wimRepository.getTeams().get(team);
        IPerson personChangeRate = wimRepository.getPersons().get(person);
        CommandValidationHelper.checkPersonNotMemberInTeam(teamToChangeStatus,personChangeRate);

        IBoard board = wimRepository.getBoards().get(boardName);
        CommandValidationHelper.checkWorkItemIdIsCorrect(id,board);
        if(!board.getBoardWorkItems().get(id).getType().equals("Story")){
            throw new IllegalArgumentException("The entered work item is not a Story");
        }
        IStory story = (IStory) board.getBoardWorkItems().get(id);
        switch (setInstructions.toLowerCase()){
            case "priority":
                story.changePriority(Priority.valueOf((value.toUpperCase())));break;
            case "size":
                story.changeStorySize(StorySize.valueOf((value.toUpperCase())));break;
            case "status":
                story.changeStoryStatus(StoryStatus.valueOf(value.toUpperCase()));break;
            default: throw new IllegalArgumentException("Incorrect instruction");
        }

        story.addHistory(String.format("%s %s was changed to %s",story.getType(), setInstructions, value));
        personChangeRate.addActivityHistory(String.format(PERSON_ACTIVITY_MESSAGE,
                person,
                setInstructions,
                story.getId(),
                value,
                LOCAL_TIME_NOW));
        board.addActivityHistory(String.format("%s of Story No.: %d in board %s was changed to %s",
                setInstructions,
                story.getId(),
                value,
                boardName));

        return String.format("%s of Story No.%d was changed",setInstructions,story.getId());
    }
}
