package com.Wim.commands;

import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;

import java.util.List;

import static com.Wim.commands.CommandConstants.*;


class CommandValidationHelper {

    static void checkTeamNotCreated(WimRepository wimRepository, String name) {
        if (!wimRepository.getTeams().containsKey(name)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE,
                    name));
        }
    }
    static void checkBoardNotCreated(WimRepository wimRepository, String board) {
        if (!wimRepository.getBoards().containsKey(board)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_FOUND_ERROR_MESSAGE,
                    board));
        }
    }
    static void checkCorrectSizeOfParameters(List<String> parameters, int correctNumberOfParameters) {
        if (parameters.size() != correctNumberOfParameters) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
    }

    static void checkPersonNotCreated(WimRepository wimRepository, String personName) {
        if (!wimRepository.getPersons().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_FOUND_ERROR_MESSAGE, personName));
        }
   }
   static void checkBoardNotBelongsToTeam(ITeam team, IBoard board){
        if (!team.getTeamBoards().contains(board)) {
        throw new IllegalArgumentException(BOARD_NOT_EXISTS_IN_TEAM_ERROR_MESSAGE);
        }
    }
    static void checkWorkItemIdIsCorrect(int workItemId, IBoard board) {
        if (workItemId < 0 || workItemId >= board.getBoardWorkItems().size()) {
            throw new IllegalArgumentException(ID_ERROR_MESSAGE);
        }
    }
    static void checkPersonNotMemberInTeam(ITeam team, IPerson person) {
        if (!team.getTeamMembers().contains(person)) {
            throw new IllegalArgumentException(PERSON_NOT_EXISTS_IN_TEAM_ERROR_MESSAGE);
        }
    }
    static void checkPersonAlreadyExists(WimRepository wimRepository, String personName) {
        if (wimRepository.getPersons().containsKey(personName)) {
           throw new IllegalArgumentException(PERSON_EXISTS_ERROR_MESSAGE);
        }
    }
    static void checkTeamAlreadyExists(WimRepository wimRepository, String teamName) {
        if (wimRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(TEAM_EXISTS_ERROR_MESSAGE);
        }
    }
}
