package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.*;

public class CreateNewTeam implements Command{
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private static final String PERSON_ACTIVITY_MESSAGE = "%s created Team %s, %s";

    private WimRepository wimRepository;
    private WimFactory wimFactory;

    public CreateNewTeam(WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personCreatingTeam = parameters.get(0);
        String teamName = parameters.get(1);
        CommandValidationHelper.checkPersonNotCreated(wimRepository,personCreatingTeam);
        return createNewTeam(personCreatingTeam, teamName);
    }

    private String createNewTeam(String personName, String teamName) {
        CommandValidationHelper.checkTeamAlreadyExists(wimRepository,teamName);
        ITeam team = wimFactory.createTeam(teamName);
        wimRepository.addTeam(teamName,team);
        IPerson person = wimRepository.getPersons().get(personName);
        team.addPersonToTeam(person);

        person.addActivityHistory(String.format(PERSON_ACTIVITY_MESSAGE,
                personName,
                teamName,
                LOCAL_TIME_NOW));
        team.addActivityHistory(String.format("Team %s was created",teamName));
        return String.format(TEAM_CREATED_SUCCESS_MESSAGE, teamName);
    }
}
