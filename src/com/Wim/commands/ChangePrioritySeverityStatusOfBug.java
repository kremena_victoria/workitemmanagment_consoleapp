package com.Wim.commands;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.enums.BugStatus;
import com.Wim.models.enums.Priority;
import com.Wim.models.enums.Severity;
import com.Wim.models.workItemContracts.IBug;

import java.time.LocalDateTime;
import java.util.List;

import static com.Wim.commands.CommandConstants.LOCAL_TIME_NOW;


public class ChangePrioritySeverityStatusOfBug implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 6;
    private static final String PERSON_ACTIVITY_MESSAGE = "%s changed %s of work item Feedback No. %d to %s, %s";

    private final WimRepository wimRepository;
    private final WimFactory wimFactory;

    public ChangePrioritySeverityStatusOfBug (WimRepository wimRepository, WimFactory wimFactory) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidationHelper.checkCorrectSizeOfParameters(parameters,CORRECT_NUMBER_OF_ARGUMENTS);
        String personToChangeStatus = parameters.get(0);
        String teamToChangeStatus = parameters.get(1);
        String boardNameToSetStatus = parameters.get(2);
        int workItemNoToSetStatus = Integer.parseInt(parameters.get(3));
        String instruction = parameters.get(4);
        String newValue = parameters.get(5);

        CommandValidationHelper.checkPersonNotCreated(wimRepository, personToChangeStatus);
        CommandValidationHelper.checkTeamNotCreated(wimRepository, teamToChangeStatus);
        CommandValidationHelper.checkBoardNotCreated(wimRepository, boardNameToSetStatus);
        return workItemToSetStatus(personToChangeStatus,
                teamToChangeStatus,
                boardNameToSetStatus,
                workItemNoToSetStatus,
                instruction,
                newValue);
    }

    private String workItemToSetStatus(String person, String team, String boardName,int id, String setInstructions, String value) {
        ITeam teamToChangeStatus = wimRepository.getTeams().get(team);
        IPerson personChangeRate = wimRepository.getPersons().get(person);
        CommandValidationHelper.checkPersonNotMemberInTeam(teamToChangeStatus,personChangeRate);

        IBoard board = wimRepository.getBoards().get(boardName);
        CommandValidationHelper.checkWorkItemIdIsCorrect(id,board);
        if(!board.getBoardWorkItems().get(id).getType().equals("Bug")){
            throw new IllegalArgumentException("The entered work item is not a Bug");
        }
        IBug bug = (IBug) board.getBoardWorkItems().get(id);
        switch (setInstructions.toLowerCase()){
            case "priority":
                bug.changePriority(Priority.valueOf((value.toUpperCase())));break;
            case "severity":
                bug.changeBugSeverity(Severity.valueOf((value.toUpperCase())));break;
            case "status":
                bug.changeBugStatus(BugStatus.valueOf(value.toUpperCase()));break;
                default: throw new IllegalArgumentException("Incorrect instruction");
        }

        bug.addHistory(String.format("%s %s was changed to %s",bug.getType(), setInstructions, value));
        personChangeRate.addActivityHistory(String.format(PERSON_ACTIVITY_MESSAGE,
                person,
                setInstructions,
                bug.getId(),
                value,
                LOCAL_TIME_NOW));
        board.addActivityHistory(String.format("%s of Bug No.: %d in board %s was changed",
                setInstructions,
                bug.getId(),
                boardName));

        return String.format("%s of Bug No.%d was changed to %s",setInstructions,bug.getId(), value);
}
}
