package com.Wim.core;

import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.*;
import com.Wim.core.factories.CommandFactoryImpl;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.core.providers.CommandParserImpl;
import com.Wim.core.providers.ConsoleReader;
import com.Wim.core.providers.ConsoleWriter;

import java.util.List;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";

    private WimFactory wimFactory;
    private CommandParser commandParser;
    private WimRepository wimRepository;
    private Writer writer;
    private Reader reader;
    private CommandWim commandWim;

    public EngineImpl() {
        wimFactory = new WimFactoryImpl();
        commandParser = new CommandParserImpl();
        writer = new ConsoleWriter();
        reader = new ConsoleReader();
        commandWim = new CommandFactoryImpl();
        wimRepository = new WimRepositoryImpl();
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);

            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }

        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandWim.createCommand(commandName, wimFactory, wimRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
