package com.Wim.core.factories;

import com.Wim.core.contracts.WimRepository;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.WorkItemBase;

import java.util.HashMap;
import java.util.Map;

public class WimRepositoryImpl implements WimRepository {
    private Map<String, IPerson> persons;
    private Map<String, ITeam> teams;
    private Map<String, IBoard> boards;
    private Map<Integer, IWorkItem> workItems;

    public WimRepositoryImpl() {
        this.persons = new HashMap<>();
        this.teams = new HashMap<>();
        this.boards = new HashMap<>();
        this.workItems = new HashMap<>();
    }

    public Map<String, IPerson> getPersons() {
        return new HashMap<>(persons);
    }

    public void addPerson(String personName, IPerson person) {
        this.persons.put(personName, person);
    }

    public Map<String, ITeam> getTeams() {
        return new HashMap<>(teams);
    }

    public void addTeam(String teamName, ITeam team) {
        this.teams.put(teamName, team);
    }

    @Override
    public Map<String, IBoard> getBoards() {
        return new HashMap<>(boards);
    }

    @Override
    public void addBoard(String boardName, IBoard board) {
        this.boards.put(boardName, board);
    }

    @Override
    public Map<Integer, IWorkItem> getWorkItems() {
        return new HashMap<>(workItems);
    }

    @Override
    public void addWorkItem(int workitemId, IWorkItem workItem) {
        this.workItems.put(workitemId, workItem);
    }
}

