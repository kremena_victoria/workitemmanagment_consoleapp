package com.Wim.core.factories;

import com.Wim.core.contracts.WimFactory;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.*;

public class WimFactoryImpl implements WimFactory {
    private static final String WORK_ITEM_ERROR_MESSAGE = "No such type workItem";

    @Override
    public IPerson createPerson(String personName) {
        return new Person(personName);
    }

    @Override
    public ITeam createTeam(String teamName) {
        return new Team(teamName);
    }

    @Override
    public IBoard createBoard(String boardName) {
        return new Board(boardName);
    }

    public IWorkItem createWorkItem(String type, String title, String description) {
        switch (type.toLowerCase()) {
            case "bug":
                return new BugImpl(title, description);
            case "story":
                return new StoryImpl(title, description);
            case "feedback":
                return new FeedbackImpl(title, description);
            default:
                throw new IllegalArgumentException(WORK_ITEM_ERROR_MESSAGE);
        }
    }

    @Override
    public CommentImpl createCommentToWorkItem(String personName, IWorkItem workItemID, String realComment) {
        return new CommentImpl(personName, workItemID, realComment);
    }
}
