package com.Wim.core.factories;

import com.Wim.commands.*;
import com.Wim.commands.contracts.Command;
import com.Wim.commands.contracts.CommandType;
import com.Wim.core.contracts.CommandWim;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;

public class CommandFactoryImpl implements CommandWim {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, WimFactory wimFactory, WimRepository wimRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());
        switch (commandType) {
            case CREATENEWPERSON:
                return new CreateNewPerson(wimRepository, wimFactory);
            case CREATENEWTEAM:
                return new CreateNewTeam(wimRepository, wimFactory);
            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(wimRepository, wimFactory);
            case SHOWALLPEOPLE:
                return new ShowAllPeople(wimRepository,wimFactory);
            case SHOWALLTEAMS:
                return new ShowAllTeams(wimRepository,wimFactory);
            case CREATENEWBOARDINTEAM:
                return new CreateNewBoardInTeam(wimRepository,wimFactory);
            case SHOWALLTEAMBOARDS:
                return new ShowAllTeamBoards(wimRepository,wimFactory);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(wimRepository,wimFactory);
            case CREATEAWORKITEM:
                return new CreateWorkItem(wimRepository,wimFactory);
            case ADDSTEPSTOREPRODUCETOBUG:
                return new AddStepsToReproduceToBug(wimRepository,wimFactory);
            case SHOWPERSONACTIVITY:
                return new ShowPersonActivity(wimRepository, wimFactory);
            case CHANGERATINGTOFEEDBACK:
                return new ChangeRatingToFeedback(wimRepository, wimFactory);
            case SHOWBOARDSACTIVITY:
                return new ShowBoardsActivity(wimRepository, wimFactory);
            case SHOWTEAMSACTIVITY:
                return new ShowTeamsActivity(wimRepository, wimFactory);
            case ASSIGNWORKITEMTOAPERSON:
                return new AssignWorkItemToAPerson(wimRepository, wimFactory);
            case UNASSIGNWORKITEMFROMAPERSON:
                return new UnAssignWorkItemFromAPerson(wimRepository, wimFactory);
            case CHANGESTATUSOFFEEDBACK:
                return new ChangeStatusOfFeedback(wimRepository, wimFactory);
            case ADDCOMMENTTOAWORKITEM:
                return new AddCommentsToAWorkItem(wimRepository, wimFactory);
            case CHANGEPRIORITYSEVERITYSTATUSOFBUG:
                return new ChangePrioritySeverityStatusOfBug(wimRepository, wimFactory);
            case CHANGEPRIORITYSIZESTATUSOFSTORY:
                return new ChangePrioritySizeStatusOfStory(wimRepository, wimFactory);
            case LISTWORKITEMS:
                return new ListWorkItems(wimRepository, wimFactory);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }
}
