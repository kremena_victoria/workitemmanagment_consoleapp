package com.Wim.core.providers;

import com.Wim.core.contracts.CommandParser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandParserImpl implements CommandParser {
    public String parseCommand(String fullCommand) {
        return fullCommand.split(" ")[0];
    }

    public List<String> parseParameters(String fullCommand) {
        List<String> matchList = new ArrayList<>();
        Pattern regex = Pattern.compile("(\"[^\"]*\"|'[^']*'|\\S+?(?:\\\\\\s+\\S*)+|\\S+)");
        Matcher regexMatcher = regex.matcher(fullCommand);
        while (regexMatcher.find()) {
            matchList.add(regexMatcher.group());
        }

        ArrayList<String> parameters = new ArrayList<>();
        for (int i = 1; i < matchList.size(); i++) {
            parameters.add(matchList.get(i));
        }
        return parameters;
    }
}
