package com.Wim.core.contracts;

import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.ActivityHistory;
import com.Wim.models.workItemContracts.Comment;
import com.Wim.models.workItemContracts.IWorkItem;

import java.util.Map;

public interface WimRepository{
    Map<String, IPerson> getPersons();
    Map<String, ITeam> getTeams();
    Map<String, IBoard> getBoards();
    Map<Integer, IWorkItem>getWorkItems();

    void addPerson(String personName, IPerson person);

    void addTeam(String teamName, ITeam team);

    void addBoard(String boardName, IBoard board);

    void addWorkItem(int workitemId, IWorkItem workItem);
}
