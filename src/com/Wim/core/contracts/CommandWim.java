package com.Wim.core.contracts;

import com.Wim.commands.contracts.Command;

public interface CommandWim {
    Command createCommand(String commandTypeAsString, WimFactory factory, WimRepository wimRepository);
}
