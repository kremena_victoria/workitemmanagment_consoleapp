package com.Wim.core.contracts;

import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.CommentImpl;
import com.Wim.models.workItemImpl.WorkItemBase;


public interface WimFactory {
    IPerson createPerson(String name);

    ITeam createTeam(String name);

    IBoard createBoard(String name);

    IWorkItem createWorkItem(String type, String title, String description);

    CommentImpl createCommentToWorkItem(String personName, IWorkItem workItemID, String realComment);
}
