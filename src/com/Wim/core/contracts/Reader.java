package com.Wim.core.contracts;

public interface Reader {
    String readLine();
}
