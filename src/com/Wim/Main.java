package com.Wim;

import com.Wim.core.EngineImpl;

public class Main {
    public static void main(String[] args) {
        EngineImpl engine = new EngineImpl();
        engine.start();
    }
}
