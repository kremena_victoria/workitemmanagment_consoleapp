package com.Wim.models.workItemContracts;

import com.Wim.models.enums.StorySize;
import com.Wim.models.enums.StoryStatus;

public interface IStory extends BugStoryBase {
    StorySize getSize();

    StoryStatus getStatus();

    void changeStorySize(StorySize size);

    void changeStoryStatus(StoryStatus status);
}
