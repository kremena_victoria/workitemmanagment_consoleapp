package com.Wim.models.workItemContracts;

import com.Wim.models.enums.BugStatus;
import com.Wim.models.enums.Severity;

import java.util.List;

public interface IBug extends BugStoryBase{
   Severity getSeverity();
    BugStatus getStatus();
    void addBugRepreduceSteps(String steps);

    void changeBugSeverity(Severity severity) ;

    void changeBugStatus(BugStatus status) ;

}
