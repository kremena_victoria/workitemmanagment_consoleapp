package com.Wim.models.workItemContracts;

import com.Wim.models.contracts.IPerson;
import com.Wim.models.enums.Priority;

public interface BugStoryBase extends IWorkItem {
    Priority getPriority();

    void changePriority(Priority priority);

    String getAssignee();

    void setAssignee(String assignee);
}
