package com.Wim.models.workItemContracts;

import com.Wim.models.enums.FeedbackStatus;

public interface IFeedback extends IWorkItem{
    void changFeedbackRating(int rating);
    void changeFeedbackStatus(FeedbackStatus status);
    int getRating();
    FeedbackStatus getStatus();

}
