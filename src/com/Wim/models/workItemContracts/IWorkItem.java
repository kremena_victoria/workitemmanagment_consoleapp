package com.Wim.models.workItemContracts;

import java.util.List;

public interface IWorkItem {
    Integer getId();

    String getTitle();

    String getDescription();

    String getType();

    void addHistory(String workItem);

}
