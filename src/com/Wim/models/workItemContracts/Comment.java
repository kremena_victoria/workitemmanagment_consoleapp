package com.Wim.models.workItemContracts;

public interface Comment {
    String getAuthor();

    String getMessage();

    void setMessage(String message);

    void setAuthor(String author);

    String getComment();


}
