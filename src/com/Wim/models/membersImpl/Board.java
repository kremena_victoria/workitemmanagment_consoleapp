package com.Wim.models.membersImpl;

import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.enums.*;
import com.Wim.models.workItemContracts.*;
import com.Wim.models.workItemImpl.common.ValidationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Board implements IBoard {
    private static final int BOARDNAME_MIN_LENGTH = 5;
    private static final int BOARDNAME_MAX_LENGTH = 10;
    private static final String BOARDNAME_LENGTH_ERROR_MESSAGE =
            "Board name should be between 5 and 10 symbols";
    private static final String WORKITEM_ERROR_MESSAGE = "no work items";
    private static final String ACTIVITY_ERROR_MESSAGE = "no activity registered";

    private String boardName;
    private List<IWorkItem> boardWorkItems;
    private List<String> boardActivityHistory;
    private List<IFeedback> boardFeedbacks;
    private List<IBug> boardBugs;
    private List<IStory> boardStories;
    private List<BugStoryBase> boardBugStories;

    public Board(String boardName) {
        setBoardName(boardName);
        boardWorkItems = new ArrayList<>();
        boardActivityHistory = new ArrayList<>();
        boardFeedbacks = new ArrayList<>();
        boardBugs = new ArrayList<>();
        boardStories = new ArrayList<>();
        boardBugStories = new ArrayList<>();
    }

    public String getBoardName() {
        return boardName;
    }

    public List<IWorkItem> getBoardWorkItems() {
        return new ArrayList<>(boardWorkItems);
    }

    public void addWorkItemToBoard(IWorkItem workItem) {
        boardWorkItems.add(workItem);
    }

    public void addBugStoryToBoard(IWorkItem workItem) {
        boardBugStories.add((BugStoryBase) workItem);
    }

    public void removeBugStoryToBoard(IWorkItem workItem) {
        if (boardBugStories.isEmpty()) {
            throw new IllegalArgumentException(WORKITEM_ERROR_MESSAGE);
        }
        boardBugStories.remove(workItem);
    }

    @Override
    public void addActivityHistory(String activityDescription) {
        boardActivityHistory.add(activityDescription);
    }

    @Override
    public String showBoardActivity() {
        StringBuilder strBuilder = new StringBuilder();
        ValidationHelper.checkListIsEmpty(strBuilder, boardActivityHistory.isEmpty(), ACTIVITY_ERROR_MESSAGE);
        strBuilder.append("List of all ");
        strBuilder.append(getBoardName());
        strBuilder.append(" activities: ");
        boardActivityHistory.forEach((val) ->
                strBuilder.append(val + ", "));
        strBuilder.append(" ");
        return String.format("%s",strBuilder);
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("Board name: ");
        strBuilder.append(getBoardName());
        return strBuilder.toString();
    }

    public String listAll() {
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb, boardWorkItems.isEmpty(), WORKITEM_ERROR_MESSAGE);
        sb.append("All work items created are: ");
        sb.append("\n");
        sb.append("---------------");
        sb.append("\n");
        boardWorkItems.forEach((work) ->
                sb.append(work.getType() + ", " + "Id.No.: " + work.getId() + " " + work.getTitle() + "\n"));
        return sb.toString();
    }

    public String filterWorkItemsByType(String type) {
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb, boardWorkItems.isEmpty(), WORKITEM_ERROR_MESSAGE);
        List<IWorkItem> item = boardWorkItems.stream()
                .filter(x -> x.getType().contains(type)).collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Type are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        return sb.toString();
    }

    public String filterWorkItemsByTitle(String title) {
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb, boardWorkItems.isEmpty(), WORKITEM_ERROR_MESSAGE);
        List<IWorkItem> item = boardWorkItems.stream()
                .filter(x -> x.getTitle().contains(title)).collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Title are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        return sb.toString();
    }

    public String filterWorkItemsByRating(Integer Rating) {
        StringBuilder sb = new StringBuilder();
        boardFeedbacks = boardWorkItems.stream()
                .filter(IFeedback.class::isInstance)
                .map(IFeedback.class::cast)
                .collect(Collectors.toList());
        ValidationHelper.checkListIsEmpty(sb, boardFeedbacks.isEmpty(), WORKITEM_ERROR_MESSAGE);
        List<IFeedback> item = boardFeedbacks.stream()
                .filter(x -> x.getRating() == (Rating)).collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Rating are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        return sb.toString();
    }

    public String filterWorkItemsByPriority(String priority) {
        Priority priority1 = Priority.valueOf(priority.toUpperCase());
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb, boardBugStories.isEmpty(), WORKITEM_ERROR_MESSAGE);
        List<BugStoryBase> item = boardBugStories.stream()
                .filter(element -> element.getPriority().equals(priority1))
                .collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Priority are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        return sb.toString();
    }

    public String filterWorkItemsByAssignee(String assignee) {
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb, boardBugStories.isEmpty(), WORKITEM_ERROR_MESSAGE);
        List<BugStoryBase> item = boardBugStories.stream()
                .filter(element -> element.getAssignee().contains(assignee))
                .collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Assignee are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        return sb.toString();
    }

    public String filterWorkItemsByStatusAndAssignee(String status, String assignee) {
        if (isInEnum(status.toUpperCase(),BugStatus.class)) {
            boardBugs = boardWorkItems.stream()
                    .filter(IBug.class::isInstance)
                    .map(IBug.class::cast)
                    .collect(Collectors.toList());
            BugStatus currentStatus = BugStatus.valueOf(status.toUpperCase());
            StringBuilder sb = new StringBuilder();
            ValidationHelper.checkListIsEmpty(sb, boardBugs.isEmpty(), WORKITEM_ERROR_MESSAGE);
            List<IBug> item = boardBugs.stream()
                    .filter(element -> element.getStatus().equals(currentStatus))
                    .filter(element -> element.getAssignee().contains(assignee))
                    .collect(Collectors.toList());
            sb.append("---------------");
            sb.append("\n");
            sb.append("Workitem after filter by Status/Assignee are:");
            item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
            return sb.toString();
        } else {
            boardStories = boardWorkItems.stream()
                    .filter(IStory.class::isInstance)
                    .map(IStory.class::cast)
                    .collect(Collectors.toList());
            StoryStatus currentStatus = StoryStatus.valueOf(status.toUpperCase());
            StringBuilder sb = new StringBuilder();
            ValidationHelper.checkListIsEmpty(sb, boardStories.isEmpty(), WORKITEM_ERROR_MESSAGE);
            List<IStory> item = boardStories.stream()
                    .filter(element -> element.getStatus().equals(currentStatus))
                    .filter(element -> element.getAssignee().contains(assignee))
                    .collect(Collectors.toList());
            sb.append("---------------");
            sb.append("\n");
            sb.append("Workitem after filter by Status and Assignee are:");
            item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
            return sb.toString();
        }
    }

    public String filterWorkItemsBySeverity(String severity) {
        boardBugs = boardWorkItems.stream()
                .filter(IBug.class::isInstance)
                .map(IBug.class::cast)
                .collect(Collectors.toList());
        Severity severity1 = Severity.valueOf(severity.toUpperCase());
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb, boardBugs.isEmpty(), WORKITEM_ERROR_MESSAGE);
        List<IBug> item = boardBugs.stream()
                .filter(element -> element.getSeverity().equals(severity1))
                .collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Severity are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        return sb.toString();
    }

    public String filterWorkItemsBySize(String size) {
        boardStories = boardWorkItems.stream()
                .filter(IStory.class::isInstance)
                .map(IStory.class::cast)
                .collect(Collectors.toList());
        StorySize size1 = StorySize.valueOf(size.toUpperCase());
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb, boardStories.isEmpty(), WORKITEM_ERROR_MESSAGE);
        List<IStory> item = boardStories.stream()
                .filter(element -> element.getSize().equals(size1))
                .collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Size are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        return sb.toString();
    }

    public String filterWorkItemsByStatus(String status) {
        StringBuilder sb = new StringBuilder();
        if (isInEnum(status.toUpperCase(), BugStatus.class)) {
            boardBugs = boardWorkItems.stream()
                    .filter(IBug.class::isInstance)
                    .map(IBug.class::cast)
                    .collect(Collectors.toList());
            BugStatus currentStatus = BugStatus.valueOf(status.toUpperCase());
            ValidationHelper.checkListIsEmpty(sb, boardBugs.isEmpty(), WORKITEM_ERROR_MESSAGE);
            List<IBug> item = boardBugs.stream()
                    .filter(element -> element.getStatus().equals(currentStatus))
                    .collect(Collectors.toList());
            sb.append("---------------");
            sb.append("\n");
            sb.append("Workitem after filter by Status are:");
            item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        }
        if (isInEnum(status.toUpperCase(), FeedbackStatus.class)) {
            boardFeedbacks = boardWorkItems.stream()
                    .filter(IFeedback.class::isInstance)
                    .map(IFeedback.class::cast)
                    .collect(Collectors.toList());
            FeedbackStatus currentStatus = FeedbackStatus.valueOf(status.toUpperCase());
            ValidationHelper.checkListIsEmpty(sb, boardFeedbacks.isEmpty(), WORKITEM_ERROR_MESSAGE);
            List<IFeedback> item = boardFeedbacks.stream()
                    .filter(element -> element.getStatus().equals(currentStatus))
                    .collect(Collectors.toList());
            sb.append("---------------");
            sb.append("\n");
            sb.append("Workitem after filter by Status are:");
            item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        }
        if (isInEnum(status.toUpperCase(), StoryStatus.class)) {
            boardStories = boardWorkItems.stream()
                    .filter(IStory.class::isInstance)
                    .map(IStory.class::cast)
                    .collect(Collectors.toList());
            StoryStatus currentStatus = StoryStatus.valueOf(status.toUpperCase());
            ValidationHelper.checkListIsEmpty(sb, boardStories.isEmpty(), WORKITEM_ERROR_MESSAGE);
            List<IStory> item = boardStories.stream()
                    .filter(element -> element.getStatus().equals(currentStatus))
                    .collect(Collectors.toList());
            sb.append("---------------");
            sb.append("\n");
            sb.append("Workitem after filter by Status are:");
            item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        }
        return sb.toString();
    }

    public <E extends Enum<E>> boolean isInEnum(String value, Class<E> enumClass) {
        for (E e : enumClass.getEnumConstants()) {
            if(e.name().equals(value)) { return true; }
        }
        return false;
    }
    private void setBoardName(String boardName) {
        ValidationHelper.checkStringLength(
                boardName,
                BOARDNAME_MIN_LENGTH,
                BOARDNAME_MAX_LENGTH,
                BOARDNAME_LENGTH_ERROR_MESSAGE);
        this.boardName = boardName;
    }
}
