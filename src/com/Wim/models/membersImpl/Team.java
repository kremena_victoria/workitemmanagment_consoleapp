package com.Wim.models.membersImpl;

import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.workItemImpl.common.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

public class Team implements ITeam {
    private static final String TEAMS_EMPTY_ERROR_MESSAGE = "no teams";
    private static final String BOARDS_EMPTY_ERROR_MESSAGE = "no boards";
    private static final String ACTIVITY_ERROR_MESSAGE = "no activities";

    private String teamName;
    private List<IPerson> teamMembers;
    private List<IBoard> teamBoards;
    private List<String>teamActivityHistory;

    public Team(String teamName){
        setTeamName(teamName);
        teamMembers = new ArrayList<>();
        teamBoards = new ArrayList<>();
        teamActivityHistory = new ArrayList<>();
    }

    public String getTeamName() { return teamName; }

    public List<IBoard> getTeamBoards() { return new ArrayList<>(teamBoards); }

    @Override
    public void addBoardToTeam(IBoard board) { teamBoards.add(board); }

    @Override
    public String boardList() {
        StringBuilder strBuilder = new StringBuilder();
        ValidationHelper.checkListIsEmpty(strBuilder,teamBoards.isEmpty(),BOARDS_EMPTY_ERROR_MESSAGE);
        strBuilder.append("Number of team boards: ");
        strBuilder.append(teamBoards.size());
        strBuilder.append("\n");
        teamBoards.forEach((board)-> strBuilder.append(board + ", "));
        return strBuilder.toString();
    }

    public List<IPerson> getTeamMembers() { return new ArrayList<>(teamMembers); }

    @Override
    public void addPersonToTeam(IPerson person) { teamMembers.add(person); }

    public String teamMembersList() {
        StringBuilder strBuilder = new StringBuilder();
        ValidationHelper.checkListIsEmpty(strBuilder,teamMembers.isEmpty(),TEAMS_EMPTY_ERROR_MESSAGE);
            strBuilder.append("Number of team members: ");
            strBuilder.append(teamMembers.size());
        strBuilder.append("\n");
        teamMembers.forEach((value)->strBuilder.append(value));
        return strBuilder.toString().trim();
    }

    @Override
    public String showTeamsActivity() {
        StringBuilder strBuilder = new StringBuilder();
        ValidationHelper.checkListIsEmpty(strBuilder, teamActivityHistory.isEmpty(), ACTIVITY_ERROR_MESSAGE);
        strBuilder.append("List of all ");
        strBuilder.append(getTeamName());
        strBuilder.append(" activities: ");
        teamActivityHistory.forEach((activity)-> strBuilder.append(activity + ", "));
        strBuilder.append(" ");
        return strBuilder.toString().trim();
    }

    @Override
    public void addActivityHistory(String activityDescription) {
        teamActivityHistory.add(activityDescription);

    }
    private void setTeamName(String teamName) { this.teamName = teamName; }
}

