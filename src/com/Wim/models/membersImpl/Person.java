package com.Wim.models.membersImpl;

import com.Wim.models.contracts.IPerson;
import com.Wim.models.workItemContracts.BugStoryBase;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.common.ValidationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Person implements IPerson {
    private static final int PERSONNAME_MIN_LENGTH = 5;
    private static final int PERSONNAME_MAX_LENGTH = 15;
    private static final String PERSONNAME_LENGTH_ERROR_MESSAGE =
            "Person's name should be between 5 and 15 symbols";
    private static final String WORKITEM_ERROR_MESSAGE = "no work items";
    private static final String ACTIVITY_ERROR_MESSAGE = "no activity registered";

    private String personName;
    private List<IWorkItem> workItems;
    private List<String> activityHistory;

    public Person(String personName) {
        setPersonName(personName);
        workItems = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public String getPersonName() {
        return personName;
    }

    @Override
    public void assignWorkItemToPerson(IWorkItem workItem) {
        workItems.add(workItem);
    }

    @Override
    public void unAssignWorkItemToPerson(IWorkItem workItem) {
        workItems.remove(workItem);
    }

    public List<IWorkItem> getWorkItems() {
        return new ArrayList<>(workItems);
    }

    public String showPersonWorkItems(){
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb,workItems.isEmpty(),"No workItems");
        workItems.forEach((val)->sb.append(val + ", "));
        return String.format("%s",sb);
    }

    @Override
    public void addActivityHistory(String activityDescription) {
        activityHistory.add(activityDescription);
    }

    public List<String> getActivityHistory() { return new ArrayList<>(activityHistory); }

    public String showActivity(){
        StringBuilder strBuilder = new StringBuilder();
        ValidationHelper.checkListIsEmpty(strBuilder, activityHistory.isEmpty(), ACTIVITY_ERROR_MESSAGE);
        strBuilder.append("List of all ");
        strBuilder.append(getPersonName());
        strBuilder.append(" ");
        strBuilder.append("activities: ");
        activityHistory.forEach((activity)->
            strBuilder.append(activity + "\n"));
            strBuilder.append(" ");
        return strBuilder.toString().trim();
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(getPersonName());
        strBuilder.append(": ");
        ValidationHelper.checkListIsEmpty(strBuilder, workItems.isEmpty(), WORKITEM_ERROR_MESSAGE);
        workItems.forEach((item)->
                strBuilder.append(item + "\n"));
        strBuilder.append("\n");
        return strBuilder.toString();
    }

    private void setPersonName(String personName) {
        ValidationHelper.checkStringLength(
                personName,
                PERSONNAME_MIN_LENGTH,
                PERSONNAME_MAX_LENGTH,
                PERSONNAME_LENGTH_ERROR_MESSAGE);
        this.personName = personName;
    }
}