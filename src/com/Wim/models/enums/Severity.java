package com.Wim.models.enums;

public enum Severity {
    CRITICAL,
    MAJOR,
    MINOR
}
