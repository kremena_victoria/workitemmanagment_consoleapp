package com.Wim.models.enums;

public enum BugStatus {
    ACTIVE{
        @Override
        public String toString() {
            return "Active";
        }
    },
    FIXED{
        @Override
        public String toString() {
            return "Fixed";
        }
    }
}
