package com.Wim.models.enums;

public enum StorySize {
    LARGE,
    MEDIUM,
    SMALL
}
