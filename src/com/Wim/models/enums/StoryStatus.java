package com.Wim.models.enums;

public enum StoryStatus {
    NOTDONE,
    INPROGRESS,
    DONE
}
