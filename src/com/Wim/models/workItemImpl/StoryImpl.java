package com.Wim.models.workItemImpl;

import com.Wim.models.enums.StorySize;
import com.Wim.models.enums.StoryStatus;
import com.Wim.models.workItemContracts.IStory;

public class StoryImpl  extends BugStoryBaseClass implements IStory {

    private StorySize size = StorySize.SMALL;
    private StoryStatus status = StoryStatus.NOTDONE;

    public StoryImpl(String title, String description){
        super(title, description);
        setSize(size);
        setStatus(status);
    }
    public StorySize getSize() {
        return size;
    }

    public StoryStatus getStatus() {
        return status;
    }

    public void setStatus(StoryStatus status) {
        this.status = status;
    }

    @Override
    public void changeStorySize(StorySize size) {
    setSize(size);
    }

    @Override
    public void changeStoryStatus(StoryStatus status) {
        setStatus(status);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("Size: ");
        sb.append(size);
        sb.append("\n");
        sb.append("Status: ");
        sb.append(status);
        sb.append("\n");
        sb.append("Priority: ");
        sb.append(getPriority());
        return super.toString() + sb.toString();
    }
    private void setSize(StorySize size) { this.size = size; }
}
