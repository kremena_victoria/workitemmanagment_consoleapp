package com.Wim.models.workItemImpl;

import com.Wim.models.workItemContracts.Comment;
import com.Wim.models.workItemContracts.IWorkItem;

public class CommentImpl implements Comment {
    private String author;
    private String message;
    IWorkItem workItemID;
    public CommentImpl(String author, IWorkItem workItemID, String message){
        setAuthor(author);
        setMessage(message);
        this.workItemID = workItemID;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getComment() {
        StringBuilder sb = new StringBuilder();
        sb.append("###################");
        sb.append("\n");
        sb.append("Comment author: ");
        sb.append(getAuthor());
        sb.append("\n");
        sb.append("Comment: ");
        sb.append(getMessage());
        sb.append("\n");
        sb.append("###################");
        return String.format("%s", sb);
    }
}
