package com.Wim.models.workItemImpl;

import com.Wim.models.workItemContracts.Comment;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.common.ValidationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class WorkItemBase implements IWorkItem {
    private static final AtomicInteger idGenerator = new AtomicInteger(0);
    private static final int TITLE_MIN_LENGTH = 10;
    private static final int TITLE_MAX_LENGTH = 50;
    private static final int DESCRIPTION_MIN_LENGTH = 10;
    private static final int DESCRIPTION_MAX_LENGTH = 500;
    private static final String TITLE_LENGTH_ERROR_MESSAGE = "Title length should be between 10 and 50 symbols";
    private static final String DESCRIPTION_LENGTH_ERROR_MESSAGE = "Description length should be between 10 and 500 symbols";
    private static final String HISTORY_EMPTY_ERROR_MESSAGE = "no history";

    private final Integer id;
    private String title;
    private String description;
    private List<String> history;

    public WorkItemBase (String title, String description){
         id = idGenerator.getAndIncrement();
         setTitle(title);
         setDescription(description);
         history = new ArrayList<>();
     }

    public Integer getId() { return id; }
    public String getTitle() { return title; }

    public String getDescription() {
        return description;
    }

    public void addHistory(String workItem){ history.add(workItem); }

    public String getType(){
        return getClass().getSimpleName().replace("Impl","");
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Work item: ");
        sb.append(getType());
        sb.append("\n");
        sb.append("Work item No.: ");
        sb.append(getId());
        sb.append("\n");
        sb.append("Title: ");
        sb.append(getTitle());
        sb.append("\n");
        sb.append("Description: ");
        sb.append(getDescription());
        ValidationHelper.checkListIsEmpty(sb,history.isEmpty(),HISTORY_EMPTY_ERROR_MESSAGE);
        sb.append("Work item History: ");
        history.forEach((val) ->
                sb.append(val + ", "));
        return sb.toString();
    }
    private void setTitle(String title) {
        ValidationHelper.checkStringLength(title,TITLE_MIN_LENGTH,
                TITLE_MAX_LENGTH,
                TITLE_LENGTH_ERROR_MESSAGE);
        this.title = title;
    }
    private void setDescription(String description) {
        ValidationHelper.checkStringLength(description, DESCRIPTION_MIN_LENGTH,
                DESCRIPTION_MAX_LENGTH,
                DESCRIPTION_LENGTH_ERROR_MESSAGE);
        this.description = description;
    }
}
