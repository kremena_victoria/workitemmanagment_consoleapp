package com.Wim.models.workItemImpl.common;

import com.Wim.models.workItemContracts.IBug;
import com.Wim.models.workItemContracts.IWorkItem;

import java.util.List;
import java.util.stream.Collectors;

public class ValidationHelper{

    public static void checkStringLength(String string, int minLength, int maxLength, String errorMessage) {
        if(string.length() < minLength || string.length() > maxLength){
            throw new IllegalArgumentException(errorMessage);
        }
    }
    public static void checkListIsEmpty(StringBuilder strBuilder, boolean empty, String errorMessage) {
        if (empty) {
            strBuilder.append(errorMessage);
        }
    }
}
