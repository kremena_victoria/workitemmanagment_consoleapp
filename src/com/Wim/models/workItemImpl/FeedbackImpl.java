package com.Wim.models.workItemImpl;

import com.Wim.models.enums.FeedbackStatus;
import com.Wim.models.workItemContracts.IFeedback;

public class FeedbackImpl extends WorkItemBase implements IFeedback {
    private static final String RATING_ERROR_MESSAGE = "Rating cannot be negative, zero or more than five";

    private int rating = 0;
    private FeedbackStatus status = FeedbackStatus.NEW;

    public FeedbackImpl(String title, String description) {
        super(title, description);
        setRating(rating);
        setStatus(status);
    }
    public int getRating() { return rating; }

    public FeedbackStatus getStatus() {
        return status;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("Feedback rating: ");
        sb.append(rating);
        sb.append("\n");
        sb.append("Status: ");
        sb.append(status);
        return super.toString() + sb.toString();
    }
    @Override
    public void changFeedbackRating(int rating) {
        setRating(rating);
    }

    @Override
    public void changeFeedbackStatus(FeedbackStatus status) {
        setStatus(status);
    }

    private void setRating(int rating) {
        checkRatingValidity(rating);
        this.rating = rating;
    }
    private void checkRatingValidity(int rating) {
        if(rating < 0 || rating > 5){
            throw new IllegalArgumentException(RATING_ERROR_MESSAGE);
        }
    }
    private void setStatus(FeedbackStatus status) { this.status = status; }
}
