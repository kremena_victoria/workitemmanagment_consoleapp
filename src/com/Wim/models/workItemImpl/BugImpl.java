package com.Wim.models.workItemImpl;

import com.Wim.models.enums.BugStatus;
import com.Wim.models.enums.Severity;
import com.Wim.models.workItemContracts.IBug;
import com.Wim.models.workItemImpl.common.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends BugStoryBaseClass implements IBug {
    private static final String BUGREPRODUCTIONSTEP_ERROR_MESSAGE = "No bug representaion steps";

    private List<String> bugRepSteps;
    private Severity severity = Severity.MINOR;
    private BugStatus status = BugStatus.ACTIVE;

    public BugImpl(String title, String description){
        super(title, description);
        this.bugRepSteps = new ArrayList<>();
        setSeverity(severity);
        setStatus(status);
    }

    public void addBugRepreduceSteps(String steps){
        bugRepSteps.add(steps);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("Bug representation Steps are: ");
        ValidationHelper.checkListIsEmpty(sb, bugRepSteps.isEmpty(), BUGREPRODUCTIONSTEP_ERROR_MESSAGE);
        bugRepSteps.forEach((val) ->
                sb.append(val + ", "));
        sb.append("\n");
        sb.append("Status: ");
        sb.append(status);
        sb.append("\n");
        sb.append("Severity: ");
        sb.append(severity);
        sb.append("\n");
        sb.append("Priority: ");
        sb.append(getPriority());
        return super.toString() + sb.toString();
    }
    public Severity getSeverity() { return severity; }

    public BugStatus getStatus() {
        return status;
    }

    @Override
    public void changeBugSeverity(Severity severity) { setSeverity(severity); }

    @Override
    public void changeBugStatus(BugStatus status) {
        setStatus(status);
    }
    private void setSeverity(Severity severity) {
        this.severity = severity;
    }
    private void setStatus(BugStatus status) {
        this.status = status;
    }
}
