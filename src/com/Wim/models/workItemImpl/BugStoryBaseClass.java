package com.Wim.models.workItemImpl;
import com.Wim.models.enums.Priority;
import com.Wim.models.workItemContracts.BugStoryBase;

public class BugStoryBaseClass extends WorkItemBase implements BugStoryBase {
    private Priority priority = Priority.LOW;
    private String assignee = "no assignee";

    public BugStoryBaseClass(String title, String description) {
        super(title, description);
        setPriority(priority);
    }

    public Priority getPriority() {
        return priority;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assingee){
       this.assignee = assingee;
    }

    public void changePriority(Priority priority) {
        setPriority(priority);
    }

    private void setPriority(Priority priority) { this.priority = priority; }
}
