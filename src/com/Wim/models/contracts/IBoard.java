package com.Wim.models.contracts;

import com.Wim.models.membersImpl.ActivityHistory;
import com.Wim.models.workItemContracts.IWorkItem;

import java.util.List;

public interface IBoard extends ActivityHistory {
        String getBoardName();

        List<IWorkItem> getBoardWorkItems();

        void addWorkItemToBoard(IWorkItem workItem);

        String showBoardActivity();

        void addBugStoryToBoard(IWorkItem workItem);

        void removeBugStoryToBoard(IWorkItem workItem);

        String listAll();
        String filterWorkItemsByType(String type);
        String filterWorkItemsByTitle(String title);
        String filterWorkItemsByRating(Integer Rating);
        String filterWorkItemsByPriority(String priority);
        String filterWorkItemsByAssignee(String assignee);
        String filterWorkItemsByStatusAndAssignee(String status, String assignee);
        String filterWorkItemsBySeverity(String severity);
        String filterWorkItemsBySize(String size);
        String filterWorkItemsByStatus(String status);
}
