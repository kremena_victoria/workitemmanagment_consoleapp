package com.Wim.models.contracts;

import com.Wim.models.membersImpl.ActivityHistory;
import java.util.List;

public interface ITeam extends ActivityHistory {
    String getTeamName();

    List<IBoard> getTeamBoards();

    List<IPerson> getTeamMembers();

    void addPersonToTeam(IPerson person);

    String teamMembersList();

    String showTeamsActivity();

    void addBoardToTeam(IBoard board);

    String boardList();

}
