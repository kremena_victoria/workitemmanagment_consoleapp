package com.Wim.models.contracts;

import com.Wim.models.membersImpl.ActivityHistory;
import com.Wim.models.workItemContracts.IWorkItem;

import java.util.List;

public interface IPerson extends ActivityHistory {
    String getPersonName();

    void assignWorkItemToPerson(IWorkItem workItem);

    void unAssignWorkItemToPerson(IWorkItem workItem);

    String showPersonWorkItems();

    List<IWorkItem> getWorkItems();

    String showActivity();

    List<String> getActivityHistory();
}
