﻿# WorkItemManagment_consoleApp
Design and implementation of  a Work Item Management (WIM) Console Application.

Feedback
Feedback has ID, title, description, rating, status, comments and history.

Title is a string between 10 and 50 symbols.
Description is a string between 10 and 500 symbols.
Rating is an integer.
Status is one of the following: New, Unscheduled, Scheduled, Done  Comments is a list of comments (string messages with author).
History is a list of all changes (string messages) that were done to the feedback.

Note: IDs of work items should be unique in the application i.e. if we have a bug with ID X then we can't have Story of Feedback with ID X.

Operations
Application should support the following operations:

Create a new person
Show all people
Show person's activity
Create a new team
Show all teams
Show team's activity
Add person to team
Show all team members
Create a new board in a team
Show all team boards
Show board's activity
Create a new Bug/Story/Feedback in a board
Change Priority/Severity/Status of a bug
Change Priority/Size/Status of a story
Change Rating/Status of a feedback
Assign/Unassign work item to a person  Add comment to a work item
List work items with options:
List all
Filter bugs/stories/feedback only
Filter by status and/or asignee
Sort by title/priority/severity/size/rating