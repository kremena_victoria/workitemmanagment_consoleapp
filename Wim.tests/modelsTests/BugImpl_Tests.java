package modelsTests;

import com.Wim.models.enums.BugStatus;
import com.Wim.models.enums.Severity;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.workItemContracts.IBug;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.WorkItemBase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BugImpl_Tests {
    private List<String> testBugRepSteps;
    private IBug testBug;


    @Before
    public void before() {

        testBug = new BugImpl("BugImplementation", "Bug description");
        testBugRepSteps = new ArrayList<>();
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyListHasNoElements() {
        List<String> myBugRepStepsList = new ArrayList<>();
        myBugRepStepsList.get(0);
    }
    @Test
    public void constructor_should_return_bugItem_when_valueIsValid() {
        // Arrange, Act
        IBug bug01 = new BugImpl("fdjfdsffkds","ldsfjldsfjdl");


        // Assert
        Assert.assertEquals(bug01.getTitle(),"fdjfdsffkds");
        Assert.assertEquals(bug01.getDescription(), "ldsfjldsfjdl");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_return_error_when_valueIsNotValid() {
        // Arrange, Act
        IBug bug01 = new BugImpl("dsffkds", "ldsfjldsfjdl");
    }
    @Test
    public void bugrepsteps_is_not_empty(){
        String steps = "fdsfdsfds";
        testBug.addBugRepreduceSteps(steps);
        testBugRepSteps.add(steps);
        Assert.assertEquals(1,testBugRepSteps.size());
    }
    @Test
    public void should_change_status(){
        String status = "Active";
        testBug.changeBugStatus(BugStatus.valueOf(status.toUpperCase()));
        Assert.assertEquals(BugStatus.ACTIVE,testBug.getStatus());
    }
    @Test
    public void should_change_severity(){
        String severity = "Critical";
        testBug.changeBugSeverity(Severity.valueOf(severity.toUpperCase()));
        Assert.assertEquals(Severity.CRITICAL,testBug.getSeverity());
    }
    @Test(expected = IllegalArgumentException.class)
    public void should_throwError_when_enum_seveirty_is_not_correct() {
        String severity = "someth";
        testBug.changeBugSeverity(Severity.valueOf(severity.toUpperCase()));
    }
    @Test
    public void testToString() {
        Assert.assertFalse(new BugImpl("fdjfdsffkds","ldsfjldsfjdl").toString().contains("@"));
    }
}


