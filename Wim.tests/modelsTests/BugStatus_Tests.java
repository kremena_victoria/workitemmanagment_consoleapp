package modelsTests;

import com.Wim.models.enums.BugStatus;
import org.junit.Assert;
import org.junit.Test;

public class BugStatus_Tests {
    @Test
    public void testToStringOfActive() {
        Assert.assertEquals(BugStatus.ACTIVE.toString(),"Active");
    }
    @Test
    public void testToStringOfFixted() {
        Assert.assertEquals(BugStatus.FIXED.toString(),"Fixed");
    }
}
