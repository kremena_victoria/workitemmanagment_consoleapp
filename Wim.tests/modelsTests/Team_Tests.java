package modelsTests;

import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.common.ValidationHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Team_Tests {
    private IPerson testTeamMember;
    private IBoard testTeamBoard;
    private String testTeamActivity;
    private ITeam testTeam;
    private List<IPerson> testTeamMembersLst;
    private List<String> testTeamActivities;

    @Before
    public void before() {
        testTeamMember = new Person("Misho");
        testTeamBoard = new Board("The saints");
        testTeam = new Team("banana bend");
        testTeamMembersLst = new ArrayList<>();
        testTeamActivities = new ArrayList<>();
    }

    @Test
    public void constructor_should_return_Team_when_valueIsValid() {
        // Arrange, Act
        Team team01 = new Team("Ad astra");

        // Assert
        Assert.assertEquals(team01.getTeamName(), "Ad astra");
    }

    @Test
    public void getTeamMembers_from_Team_should_returnShallowCopy() {
        // Arrange
        Team team01 = new Team("Ad astra");

        // Act
        List<IPerson> supposedShallowCopy = team01.getTeamMembers();
        team01.addPersonToTeam(testTeamMember);

        // Assert
        Assert.assertEquals(0, supposedShallowCopy.size());
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListHasNoMembers(){
        List<IPerson> teamMembers = new ArrayList<>();
        teamMembers.get(0);
    }

    @Test
    public void getTeamBoards_from_Team_should_returnShallowCopy() {
        // Arrange
        Team team01 = new Team("Ad astra");

        // Act
        List<IBoard> supposedShallowCopy = team01.getTeamBoards();
        team01.addBoardToTeam(testTeamBoard);

        // Assert
        Assert.assertEquals(0, supposedShallowCopy.size());
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListHasNoBoards() {
        List<IBoard> teamBoards = new ArrayList<>();
        teamBoards.get(0);
    }
    @Test
    public void should_show_Error_Message_when_boardList_is_empty() {
        Assert.assertEquals("no boardsNumber of team boards: 0"+"\n", testTeam.boardList());
    }
    @Test
    public void should_show_Error_Message_when_teamActivityList_is_empty() {
        Assert.assertEquals("no activitiesList of all banana bend activities:", testTeam.showTeamsActivity());
    }
    @Test
    public void should_show_Error_Message_when_teamMemberList_is_empty() {
        Assert.assertEquals("no teamsNumber of team members: 0", testTeam.teamMembersList());
    }
    @Test
    public void should_show_teamMemberList_when_pass(){
        IPerson person1 = new Person("Pesho");
        testTeamMembersLst.add(testTeamMember);
        testTeamMembersLst.add(person1);
        StringBuilder sb = new StringBuilder();
        testTeamMembersLst.forEach((value)->sb.append(value));
        Assert.assertEquals("Misho: no work items" +"\n" + "Pesho: no work items"+"\n",sb.toString());
    }
    @Test
    public void should_show_teamActivityList_when_pass(){
    String activity1 = "jdfjjfj";
    String activity2 = "jfjfjjd";
    testTeamActivities.add(activity1);
    testTeamActivities.add(activity2);
        StringBuilder strBuilder = new StringBuilder();
    testTeamActivities.forEach((activity)->
            strBuilder.append(activity + ", "));
        Assert.assertEquals("jdfjjfj, " + "jfjfjjd, ",strBuilder.toString());
    }


}
