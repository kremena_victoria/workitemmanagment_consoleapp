package modelsTests;

import com.Wim.models.enums.BugStatus;
import com.Wim.models.enums.Priority;
import org.junit.Assert;
import org.junit.Test;

public class Priority_Tests {
    @Test
    public void testToStringOfHigh() {
        Assert.assertEquals(Priority.HIGH.toString(),"High");
    }
    @Test
    public void testToStringOfLow() {
        Assert.assertEquals(Priority.LOW.toString(),"Low");
    }
    @Test
    public void testToStringOfMedium() {
        Assert.assertEquals(Priority.MEDIUM.toString(), "Medium");
    }
}
