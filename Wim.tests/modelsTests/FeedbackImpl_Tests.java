package modelsTests;

import com.Wim.models.enums.FeedbackStatus;
import com.Wim.models.workItemContracts.IFeedback;
import com.Wim.models.workItemImpl.FeedbackImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FeedbackImpl_Tests {
    private int rating;
    private IFeedback testFeedback;
    @Before
    public void before() {
        testFeedback = new FeedbackImpl("BugImplementation", "Bug description");
    }
    @Test(expected = IllegalArgumentException.class)
    public void change_rating_should_throwError_when_sizeIsNotPositive() {
           rating = -2;
       testFeedback.changFeedbackRating(rating);
    }
    @Test(expected = IllegalArgumentException.class)
    public void change_rating_should_throwError_when_sizeMoreThanFive() {
        rating = 6;
        testFeedback.changFeedbackRating(rating);
    }
    @Test
    public void should_change_rating_when_valid() {
        rating = 3;
        testFeedback.changFeedbackRating(rating);
        Assert.assertEquals(3,testFeedback.getRating());
    }
    @Test
    public void constructor_should_return_FeedbackItem_when_valueIsValid() {
        // Arrange, Act
        IFeedback feedback = new FeedbackImpl("fdjfdsffkds", "ldsfjldsfjdl");


        // Assert
        Assert.assertEquals(feedback.getTitle(), "fdjfdsffkds");
        Assert.assertEquals(feedback.getDescription(), "ldsfjldsfjdl");
    }
    @Test
    public void should_change_status() {
        String status = "Done";
        testFeedback.changeFeedbackStatus(FeedbackStatus.valueOf(status.toUpperCase()));
        Assert.assertEquals(FeedbackStatus.DONE, testFeedback.getStatus());
    }
    @Test
    public void testToString() {
        Assert.assertFalse(new FeedbackImpl("fdjfdsffkds","ldsfjldsfjdl").toString().contains("@"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void should_throwError_when_enum_status_get_incorrect_input() {
        String status = "someth";
        testFeedback.changeFeedbackStatus(FeedbackStatus.valueOf(status.toUpperCase()));
    }
}
