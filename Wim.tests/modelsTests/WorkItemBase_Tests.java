package modelsTests;

import com.Wim.models.contracts.IPerson;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.StoryImpl;
import com.Wim.models.workItemImpl.WorkItemBase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class WorkItemBase_Tests {

    private List<String> testHistoryElement;
    private IWorkItem testWorkItem;

    @Before
    public void before() {
        testWorkItem = new WorkItemBase("ljdslfjffkkkkk", "dfjsdlfjdflc");
        testHistoryElement =  new ArrayList<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_workItemNameIsTooShort() {
        IWorkItem workItem01 = new WorkItemBase("shor","jfdsfhds");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_workItemNameIsTooLong() {
        IWorkItem workItem01 = new WorkItemBase("tooooooooooo looooooooooooooong title","jfdsfhds");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_workItem_Description_IsTooShort() {
        IWorkItem workItem01 = new WorkItemBase("Bug012","short_d");
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void constructor_should_throwError_when_workItem_Description_IsTooLong() {
//        IWorkItem workItem01 = new WorkItemBase("WorkItemTitle","jfdsdddddddddd" +
//                "ddddddddddddddddffffffffffffffffffffffffffffffffffffffffffff" +
//                "ffffdddddddddddddddddddddddfhghhhhhhhhhhhhfffffffffffffffffdsjfdf;dsfksd'ffdflfdkj;" +
//                ";;;;;;dffffffffffffffffffdsffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff" +
//                "ffffffffffffffffffffffffffffffffffffffffdfffffffffffffffffffffffffffffffffffffffff");
//    }

    @Test
    public void constructor_should_returnWorkItem_when_valueIsValid() {
        // Arrange, Act
        IWorkItem workItem01 = new WorkItemBase("Workitem title","Workitem description");
        // Assert
        Assert.assertEquals(workItem01.getDescription(), "Workitem description");
        Assert.assertEquals(workItem01.getTitle(), "Workitem title");
    }
    @Test
    public void getHistory_from_WorkItemList_should_returnShallowCopy() {
        // Act
       String testHistory = "lfjdslfjsdjf";
        testWorkItem.addHistory(testHistory);
        testHistoryElement.add(testHistory);

        // Assert
        Assert.assertEquals(1, testHistoryElement.size());
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListHasNoElements(){
        List<String> myHistoryList = new ArrayList<>();
        myHistoryList.get(0);
    }
    @Test
    public void testToString() {
        Assert.assertFalse(new WorkItemBase("fdjfdsffkds","ldsfjldsfjdl").toString().contains("@"));
    }


}
