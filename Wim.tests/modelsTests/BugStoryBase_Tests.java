package modelsTests;

import com.Wim.models.contracts.IPerson;
import com.Wim.models.enums.BugStatus;
import com.Wim.models.enums.Priority;
import com.Wim.models.enums.Severity;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.workItemContracts.BugStoryBase;
import com.Wim.models.workItemContracts.IBug;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.BugStoryBaseClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class BugStoryBase_Tests {

    private String testAssignee;
    private BugStoryBase testBugStory;

    @Before
    public void before() {
        testAssignee = "Tomas";
        testBugStory = new BugStoryBaseClass("ljdslfjffkkkkk", "dfjsdlfjdflc");
    }

    @Test
    public void constructor_should_return_bugStoryItem_when_valueIsValid() {
        // Arrange, Act
        BugStoryBase bugStory01 = new BugStoryBaseClass("fdjfdsffkds", "ldsfjldsfjdl");


        // Assert
        Assert.assertEquals(bugStory01.getTitle(), "fdjfdsffkds");
        Assert.assertEquals(bugStory01.getDescription(), "ldsfjldsfjdl");
    }
    @Test
    public void getAssignee_work_properly() {
        // Arrange
        BugStoryBase bugOrStory01 = new BugStoryBaseClass("BugOrStory01", "description of bug or story");

        // Act
        bugOrStory01.setAssignee(testAssignee);
//
        // Assert
        Assert.assertEquals(testAssignee, bugOrStory01.getAssignee());
    }

    @Test
    public void should_change_priority_toHigh() {
        String priority = "High";
        testBugStory.changePriority(Priority.valueOf(priority.toUpperCase()));
        Assert.assertEquals(Priority.HIGH, testBugStory.getPriority());
    }
    @Test(expected = IllegalArgumentException.class)
    public void should_throwError_when_enum_priority_get_incorrect_input() {
        String priority = "someth";
        testBugStory.changePriority(Priority.valueOf(priority.toUpperCase()));
    }
    @Test
    public void should_change_priority_toLow() {
        String priority = "Low";
        testBugStory.changePriority(Priority.valueOf(priority.toUpperCase()));
        Assert.assertEquals(Priority.LOW, testBugStory.getPriority());
    }
}