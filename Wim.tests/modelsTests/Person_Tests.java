package modelsTests;

import com.Wim.models.contracts.IPerson;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.workItemContracts.IStory;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.CommentImpl;
import com.Wim.models.workItemImpl.StoryImpl;
import com.Wim.models.workItemImpl.WorkItemBase;
import com.Wim.models.workItemImpl.common.ValidationHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Person_Tests {
    private IWorkItem testWorkItem;
    private IPerson testPerson;
    private List<IWorkItem> testWorkItems;
    private List<String> testActivityHistory;

    @Before
    public void before() {
        testWorkItem = new BugImpl("BugImplementation", "Bug description");
        testPerson = new Person("Misho");
        testWorkItems = new ArrayList<>();
        testActivityHistory = new ArrayList<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_personNameIsTooShort() {
        Person person01 = new Person("kim");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_personNameIsTooLong() {
        Person person01 = new Person("PIPILOTA VICTORIA ESPERANZA DALGOTO CHORAPCHE");
    }

    @Test
    public void constructor_should_returnPerson_when_valueIsValid() {
        // Arrange, Act
        Person person01 = new Person("Kimberly");

        // Assert
        Assert.assertEquals(person01.getPersonName(), "Kimberly");
    }

    @Test
    public void getWorkItems_should_returnShallowCopy() {
        // Arrange
        Person person01 = new Person("Kimberly");

        // Act
        List<IWorkItem> supposedShallowCopy = person01.getWorkItems();
        person01.assignWorkItemToPerson(testWorkItem);

        // Assert
        Assert.assertEquals(0, supposedShallowCopy.size());
    }

    @Test
    public void unAssign_workItem_from_purson_succesfully() {
        // Arrange
        Person person01 = new Person("Kimberly");

        person01.assignWorkItemToPerson(testWorkItem);
        // Act
        person01.unAssignWorkItemToPerson(testWorkItem);

        // Assert
        Assert.assertEquals(0, person01.getWorkItems().size());
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListOfWorkItems() {
        testWorkItems.get(0);
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListOfActivity(){
        List<String> activity = new ArrayList<>();
        activity.get(0);
    }
    @Test
    public void should_add_Activity_toActivityList() {
        String activity = "fdfjdsjfffff";
        testPerson.addActivityHistory(activity);
        testActivityHistory.add(activity);
        // Assert
        Assert.assertEquals(1, testPerson.getActivityHistory().size());
    }
    @Test
    public void should_show_Error_Message_when_Person_WorkItems_are_empty() {
        Assert.assertEquals("No workItems", testPerson.showPersonWorkItems());
    }
    @Test
    public void should_show_Error_Message_when_Person_activities_are_empty() {
        testPerson.getPersonName();
        Assert.assertEquals("no activity registeredList of all Misho activities:", testPerson.showActivity());
    }
    @Test
    public void testToString() {
        Assert.assertFalse(testPerson.toString().contains("@"));
    }
    @Test
    public void should_show_personActivityList_when_pass(){
        String activity1 = "jdfjjfj";
        String activity2 = "jfjfjjd";
        testActivityHistory.add(activity1);
        testActivityHistory.add(activity2);
        testPerson.addActivityHistory(activity1);
        testPerson.addActivityHistory(activity2);
        StringBuilder strBuilder = new StringBuilder();
        testPerson.getActivityHistory().forEach((activity)->
                strBuilder.append(activity + "\n"));
        strBuilder.append(" ");
        Assert.assertEquals("List of all Misho activities: jdfjjfj"+"\n"+ "jfjfjjd",testPerson.showActivity());
    }
    @Test
    public void should_toString_personWorkItemList_when_pass() {
        IStory story = new StoryImpl("flkdjflaaaa", "asd;fjdsjfdsjafjdsakjf");
        testPerson.assignWorkItemToPerson(story);
        testWorkItems.add(story);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(testPerson.getPersonName());
        strBuilder.append(": ");
        testWorkItems.forEach((item) ->
                strBuilder.append(item + "\n"));
        strBuilder.append("\n");
        Assert.assertEquals(strBuilder.toString(), testPerson.toString());

    }




}
