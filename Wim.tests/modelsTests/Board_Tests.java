package modelsTests;

import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.enums.BugStatus;
import com.Wim.models.enums.Priority;
import com.Wim.models.enums.StorySize;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.workItemContracts.*;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.BugStoryBaseClass;
import com.Wim.models.workItemImpl.StoryImpl;
import com.Wim.models.workItemImpl.WorkItemBase;
import com.Wim.models.workItemImpl.common.ValidationHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Board_Tests {
    private IWorkItem testWorkItem;
    private IBoard testBoard;
    private List<IWorkItem> workItemlst;
    private List<BugStoryBase> testBugStories;
    private List<String> testActivities;
    private List<IBug> testBuglst;
    @Before
    public void before() {

        testWorkItem = new BugImpl("BugImplementation", "Bug description");
        testBugStories = new ArrayList<>();
        testActivities = new ArrayList<>();
        testBoard = new Board("djfdlsj");
        workItemlst = new ArrayList<>();
        testBuglst = new ArrayList<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_boardNameIsTooShort() {

        Board board01 = new Board("Kiwi");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_boardNameIsTooLong() {
        Board board01 = new Board("The strongest ninjas");
    }

    @Test
    public void constructor_should_return_board_when_valueIsValid() {
        // Arrange, Act
        Board board01 = new Board("The Saints");

        // Assert
        Assert.assertEquals(board01.getBoardName(), "The Saints");
    }
    @Test
    public void show_revert_true_if_list_is_empty (){
        Assert.assertTrue(workItemlst.isEmpty());


    }

    @Test
    public void getWorkItems_from_Board_should_returnShallowCopy() {
        // Arrange
        Board board01 = new Board("The Saints");

        // Act
        List<IWorkItem> supposedShallowCopy = new ArrayList<>();
        board01.addWorkItemToBoard(testWorkItem);
        supposedShallowCopy.add(testWorkItem);

        // Assert
        Assert.assertEquals(1, board01.getBoardWorkItems().size());
    }
    @Test
    public void add_should_addWorkitem_to_BugstoryList_when_passed() {
        // Arrange
        Board board01 = new Board("The Saints");
        BugStoryBase testBugStory = new BugStoryBaseClass("dlfjdslfjd","kfds;fjdsfjfdfjf");
        board01.addBugStoryToBoard(testBugStory);
        testBugStories.add(testBugStory);
        board01.addBugStoryToBoard(testBugStory);
        testBugStories.add(testBugStory);
        board01.removeBugStoryToBoard(testBugStory);
        testBugStories.remove(testBugStory);
        // Assert
        Assert.assertEquals(1, testBugStories.size());
    }
        @Test
    public void should_show_Error_Message_when_show_All_activities_and_boarActivityList_is_empty() {
        Assert.assertEquals("no activity registeredList of all djfdlsj activities:  ",
                testBoard.showBoardActivity());
    }
    @Test
    public void should_show_Error_Message_when_show_All_boardItems_and_boarItemsList_is_empty() {
        Assert.assertEquals("no work itemsAll work items created are: "+"\n"+"---------------"+"\n",
                testBoard.listAll());
    }
    @Test
    public void should_show_Error_Message_when_workItemList_is_empty_when_filter_By_type() {
        Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Type are:"
                        , testBoard.filterWorkItemsByType("Bug"));
    }
    @Test
    public void should_show_Error_Message_when_boardStoryList_is_empty_when_filter_By_size() {
               Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Size are:"
                       , testBoard.filterWorkItemsBySize("small"));
    }
    @Test
    public void should_show_Error_Message_when_workItmelist_is_empty_when_filter_By_status() {
        Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Status are:"
                        +"no work items"
                        +"---------------"
                        +"\n"
                        +"Workitem after filter by Status are:"
                        , testBoard.filterWorkItemsByStatus("done"));
    }

    @Test
    public void should_show_Error_Message_when_BugList_is_empty_when_filter_By_Severity() {
        Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Severity are:"
                        , testBoard.filterWorkItemsBySeverity("critical"));
    }
    @Test
    public void should_show_Error_Message_when_FeedbackList_is_empty_when_filter_By_Rating() {
        Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Rating are:"
                        , testBoard.filterWorkItemsByRating(2));
    }
    @Test
    public void should_show_Error_Message_when_BugStoryList_is_empty_when_filter_By_Priority() {
        Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Priority are:"
                        , testBoard.filterWorkItemsByPriority("High"));
    }
    @Test
    public void should_show_Error_Message_when_workItemList_is_empty_when_filter_By_Title() {
        Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Title are:"
                        , testBoard.filterWorkItemsByTitle("problem"));
    }
    @Test
    public void should_show_Error_Message_when_BugStoryList_is_empty_when_filter_By_Assignee() {
        Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Assignee are:"
                        , testBoard.filterWorkItemsByAssignee("Misho"));
    }
    @Test
    public void should_show_Error_Message_when_BugStoryList_is_empty_when_filter_By_AssigneeAndStatus() {
        Assert.assertEquals("no work items"
                        + "---------------"
                        +"\n"
                        +"Workitem after filter by Status and Assignee are:"
                        , testBoard.filterWorkItemsByStatusAndAssignee("done","Misho"));
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListOfWorkItemHasNoElements(){
        List<IWorkItem> myWorkItemList = new ArrayList<>();
        myWorkItemList.get(0);
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListOfFeedbacksHasNoElements(){
        List<IFeedback> myWorkItemList = new ArrayList<>();
        myWorkItemList.get(0);
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListOfBugsHasNoElements(){
        List<IBug> myWorkItemList = new ArrayList<>();
        myWorkItemList.get(0);
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListOfBugStoryBaseHasNoElements(){
        List<BugStoryBase> myWorkItemList = new ArrayList<>();
        myWorkItemList.get(0);
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListOfStoryHasNoElements(){
        List<IStory> myWorkItemList = new ArrayList<>();
        myWorkItemList.get(0);
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testEmptyListOfActivityHasNoElements(){
        List<String> myWorkItemList = new ArrayList<>();
        myWorkItemList.get(0);
    }
    @Test
    public void testToString() {
        Assert.assertFalse(testBoard.toString().contains("@"));
    }
    @Test
    public void should_add_only_Stories_To_BoardStories() {
        IBug bug = new BugImpl("newBugCreated","ne raboti dobre");
        IStory story = new StoryImpl("newStoryCreated", "new feature is added");
        workItemlst.add(bug);
        workItemlst.add(story);
        List<IStory> boardStories;
        boardStories = workItemlst.stream()
                .filter(IStory.class::isInstance)
                .map(IStory.class::cast)
                .collect(Collectors.toList());
        Assert.assertEquals(workItemlst.get(1), boardStories.get(0));
    }
    @Test
    public void should_return_True_when_Pass() {
        String value = "HIGH";
        Assert.assertTrue(isInEnum(value,Priority.class));
    }

    public <E extends Enum<E>> boolean isInEnum(String value, Class<E> enumClass) {
        for (E e : enumClass.getEnumConstants()) {
            if(e.name().equals(value)) { return true; }
        }
        return false;
    }
    @Test
    public void should_show_result_of_filter_byStatusAndByAssignee() {
        IBug newBug = new BugImpl("BugForTest", "should test somth");
        testBoard.addWorkItemToBoard(newBug);
        workItemlst.add(newBug);
        IPerson perston01 = new Person("Misho");
        perston01.assignWorkItemToPerson(newBug);
        String status = "Active";
        String assignee = "Misho";
        testBuglst = workItemlst.stream()
                .filter(IBug.class::isInstance)
                .map(IBug.class::cast)
                .collect(Collectors.toList());
        BugStatus currentStatus = BugStatus.valueOf(status.toUpperCase());
        StringBuilder sb = new StringBuilder();
        ValidationHelper.checkListIsEmpty(sb, testBuglst.isEmpty(), "No work item");
        List<IBug> item = testBuglst.stream()
                .filter(element -> element.getStatus().equals(currentStatus))
                .filter(element -> element.getAssignee().contains(assignee))
                .collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Status/Assignee are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        Assert.assertEquals(sb.toString(), testBoard.filterWorkItemsByStatusAndAssignee(status, assignee));
    }
    @Test
    public void should_show_result_of_filter_byStatus() {
        IBug newBug = new BugImpl("BugForTest", "should test somth");
        testBoard.addWorkItemToBoard(newBug);
        workItemlst.add(newBug);
        String status = "Active";
        StringBuilder sb = new StringBuilder();
        testBuglst = workItemlst.stream()
                .filter(IBug.class::isInstance)
                .map(IBug.class::cast)
                .collect(Collectors.toList());
        BugStatus currentStatus = BugStatus.valueOf(status.toUpperCase());
        ValidationHelper.checkListIsEmpty(sb, testBuglst.isEmpty(), "No work item");
        List<IBug> item = testBuglst.stream()
                .filter(element -> element.getStatus().equals(currentStatus))
                .collect(Collectors.toList());
        sb.append("---------------");
        sb.append("\n");
        sb.append("Workitem after filter by Status are:");
        item.forEach((x) -> sb.append("\n" + ("---------------") + "\n" + (x)));
        Assert.assertEquals(sb.toString(), testBoard.filterWorkItemsByStatus(status));
    }
}
