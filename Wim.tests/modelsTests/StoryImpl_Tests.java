package modelsTests;

import com.Wim.models.enums.StorySize;
import com.Wim.models.enums.StoryStatus;
import com.Wim.models.workItemContracts.IStory;
import com.Wim.models.workItemImpl.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StoryImpl_Tests {
    private IStory testStory;

    @Before
    public void before() {
        testStory = new StoryImpl("ljdslfjffkkkkk", "dfjsdlfjdflc");
    }

    @Test
    public void constructor_should_return_bugStoryItem_when_valueIsValid() {
        // Arrange, Act
        IStory story01 = new StoryImpl("fdjfdsffkds", "ldsfjldsfjdl");


        // Assert
        Assert.assertEquals(story01.getTitle(), "fdjfdsffkds");
        Assert.assertEquals(story01.getDescription(), "ldsfjldsfjdl");
    }

    @Test
    public void should_change_size_toSmall() {
        String size = "Small";
        testStory.changeStorySize(StorySize.valueOf(size.toUpperCase()));
        Assert.assertEquals(StorySize.SMALL, testStory.getSize());
    }
    @Test
    public void should_change_Status_toLow() {
        String status = "NotDone";
        testStory.changeStoryStatus(StoryStatus.valueOf(status.toUpperCase()));
        Assert.assertEquals(StoryStatus.NOTDONE, testStory.getStatus());
    }
    @Test
    public void testToString() {
        Assert.assertFalse(new StoryImpl("fdjfdsffkds","ldsfjldsfjdl").toString().contains("@"));
    }
}
