package commandsTests;

import com.Wim.commands.ListWorkItems;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListWorkItems_Tests {

    private Command testCommand;
    private IPerson testPersonAssignee;
    private ITeam testTeam;
    private IBoard testBoard;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ListWorkItems(wimRepository, wimFactory);
        testPersonAssignee = new Person("Misho");
        testTeam = new Team("The Saints");
        testBoard = new Board("board01");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_when_passed_Valid_Arguments() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01", "list all", "0", "Misho").
                collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        wimRepository.addTeam("The Saints", team);

        IBoard board = wimFactory.createBoard(testList.get(1));
        wimRepository.addBoard("board01", board);
        team.addBoardToTeam(board);

        String instruction = "list all";

        IPerson assignee = wimFactory.createPerson(testList.get(4));
        wimRepository.addPerson("Misho", assignee);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals(1, wimRepository.getPersons().size());
    }

    @Test
    public void execute_when_passed_Valid_Rating01() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01", "filter bugs", "0", "Misho").
                collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        wimRepository.addTeam("The Saints", team);

        IBoard board = wimFactory.createBoard(testList.get(1));
        wimRepository.addBoard("board01", board);
        team.addBoardToTeam(board);

        String instruction = "list all";
        String testRating = testList.get(2);

        IPerson assignee = wimFactory.createPerson(testList.get(4));
        wimRepository.addPerson("Misho", assignee);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals("filter bugs", testRating);
    }

    @Test
    public void execute_when_passed_Valid_Rating02() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01", "filter stories", "0", "Misho").
                collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        wimRepository.addTeam("The Saints", team);

        IBoard board = wimFactory.createBoard(testList.get(1));
        wimRepository.addBoard("board01", board);
        team.addBoardToTeam(board);

        String instruction = "list all";
        String testRating = testList.get(2);

        IPerson assignee = wimFactory.createPerson(testList.get(4));
        wimRepository.addPerson("Misho", assignee);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals("filter stories", testRating);
    }

    @Test
    public void execute_when_passed_Valid_Rating03() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01", "filter feedback", "0", "Misho").
                collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        wimRepository.addTeam("The Saints", team);

        IBoard board = wimFactory.createBoard(testList.get(1));
        wimRepository.addBoard("board01", board);
        team.addBoardToTeam(board);

        String instruction = "list all";
        String testRating = testList.get(2);

        IPerson assignee = wimFactory.createPerson(testList.get(4));
        wimRepository.addPerson("Misho", assignee);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals("filter feedback", testRating);
    }

    @Test
    public void execute_when_passed_Valid_Rating04() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01", "filter title", "0", "Misho").
                collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        wimRepository.addTeam("The Saints", team);

        IBoard board = wimFactory.createBoard(testList.get(1));
        wimRepository.addBoard("board01", board);
        team.addBoardToTeam(board);

        String instruction = "list all";
        String testRating = testList.get(2);

        IPerson assignee = wimFactory.createPerson(testList.get(4));
        wimRepository.addPerson("Misho", assignee);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals("filter title", testRating);
    }

    @Test
    public void execute_when_passed_Valid_Rating05() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01", "filter rating", "0", "Misho").
                collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        wimRepository.addTeam("The Saints", team);

        IBoard board = wimFactory.createBoard(testList.get(1));
        wimRepository.addBoard("board01", board);
        team.addBoardToTeam(board);

        String instruction = "list all";
        String testRating = testList.get(2);

        IPerson assignee = wimFactory.createPerson(testList.get(4));
        wimRepository.addPerson("Misho", assignee);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals("filter rating", testRating);
    }

    @Test
    public void execute_when_passed_Valid_Rating06() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01", "filter assignee", "0", "Misho").
                collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        wimRepository.addTeam("The Saints", team);

        IBoard board = wimFactory.createBoard(testList.get(1));
        wimRepository.addBoard("board01", board);
        team.addBoardToTeam(board);

        String instruction = "list all";
        String testRating = testList.get(2);

        IPerson assignee = wimFactory.createPerson(testList.get(4));
        wimRepository.addPerson("Misho", assignee);
        team.addPersonToTeam(assignee);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals("filter assignee", testRating);
    }

    @Test
    public void execute_when_passed_Valid_Rating09() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01", "filter status", "0", "Misho").
                collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        wimRepository.addTeam("The Saints", team);

        IBoard board = wimFactory.createBoard(testList.get(1));
        wimRepository.addBoard("board01", board);
        team.addBoardToTeam(board);

        String instruction = "list all";
        String testRating = testList.get(2);

        IPerson assignee = wimFactory.createPerson(testList.get(4));
        wimRepository.addPerson("Misho", assignee);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals("filter status", testRating);
    }

}
