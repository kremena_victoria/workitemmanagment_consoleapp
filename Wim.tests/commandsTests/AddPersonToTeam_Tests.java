package commandsTests;

import com.Wim.commands.AddPersonToTeam;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddPersonToTeam_Tests {
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private AddPersonToTeam commandToTest;
    private List<String> parameters;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        commandToTest = new AddPersonToTeam(wimRepository, wimFactory);
        parameters = Stream.of("personWhoAdded", "TeamToAddPerson", "personWhoIsAdded").collect(Collectors.toList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        parameters.add("person01");
        parameters.add("team01");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        // Arrange
        parameters.add("person01");
        parameters.add("team01");
        parameters.add("addedperson");
        parameters.add("dsfdf");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfTeamDoesNotExist() {
        parameters = new ArrayList<>();
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_Exception_IfPersonNotExists() {
        IPerson person = new Person("Person01");
        wimRepository.addPerson("Person01", person);
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionArguments_IfTeamNotExists() {
        ITeam team = new Team("teamName01");
        wimRepository.addTeam("teamName01", team);
        commandToTest.execute(parameters);

    }
    @Test
    public void execute_should_addComments_when_inputIsValid() {
        // Arrange
        IPerson person01 = new Person("Joanis");
        wimRepository.addPerson("Joanis", person01);
        String personName01 = person01.getPersonName();
        ITeam team01 = new Team("The dragons");
        wimRepository.addTeam("The dragons", team01);
        String teamName = team01.getTeamName();
        team01.addPersonToTeam(person01);
        IPerson person02 = new Person("Pesho");
        wimRepository.addPerson("Pesho", person02);
        String personName02 = person02.getPersonName();
        parameters = new ArrayList<>();
        parameters.add(personName01);
        parameters.add(teamName);
        parameters.add(personName02);

        // Act
        commandToTest.execute(parameters);
    }
}
