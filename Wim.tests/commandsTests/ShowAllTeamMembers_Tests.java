package commandsTests;

import com.Wim.commands.AddPersonToTeam;
import com.Wim.commands.ShowAllTeamMembers;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemImpl.common.ValidationHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShowAllTeamMembers_Tests {

    private Command testCommand;
    private IPerson testPerson01;
    private IPerson testPerson02;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ShowAllTeamMembers(wimRepository, wimFactory);
        testPerson01 = new Person("Misho");
        testPerson02 = new Person("Karolina");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "board01").collect(Collectors.toList());

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passeTheseArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        ITeam team = new Team("The Saints");
        wimRepository.addTeam("The Saints", team);

        team.addPersonToTeam(testPerson01);
        team.addPersonToTeam(testPerson02);

        // Act & Assert
        testCommand.execute(testList);
    }

}
