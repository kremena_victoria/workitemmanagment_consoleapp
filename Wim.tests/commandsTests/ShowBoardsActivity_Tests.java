package commandsTests;

import com.Wim.commands.CreateNewBoardInTeam;
import com.Wim.commands.ShowBoardsActivity;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShowBoardsActivity_Tests {

    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private IBoard testBoard;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ShowBoardsActivity(wimRepository, wimFactory);
        testBoard = new Board("boardName");
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfTeamDoesNotExist() {
        List<String> testList = new ArrayList<>();
        testCommand.execute(testList);
    }

    @Test
    public void execute_when_passed_Valid_Arguments() {
        // Arrange
        List<String> testList = Stream.of("board01").collect(Collectors.toList());

        IBoard board01 = wimFactory.createBoard(testList.get(0));
        wimRepository.addBoard("board01", board01);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals(1, wimRepository.getBoards().size());
    }



}

