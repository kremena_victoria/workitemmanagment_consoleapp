package commandsTests;

import com.Wim.commands.AddCommentsToAWorkItem;
import com.Wim.commands.ChangePrioritySeverityStatusOfBug;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChangePrioritySeverityStatusOfBug_Tests {

    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private ChangePrioritySeverityStatusOfBug commandToTest;
    private List<String> parameters;
    private String testComment;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testComment = "Test comment";
        commandToTest = new ChangePrioritySeverityStatusOfBug(wimRepository, wimFactory);
        parameters = Stream.of("Pesho", "The Saints", "board01", String.valueOf(0), "instructions", "value").collect(Collectors.toList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        parameters.add("Pesho");
        parameters.add("The Saints");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        // Arrange
        parameters.add("Pesho");
        parameters.add("The Saints");
        parameters.add("board01");
        parameters.add("0");
        parameters.add("instructions");
        parameters.add("value");
        parameters.add("dfgdfsgd");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfTeamDoesNotExist() {
        parameters = new ArrayList<>();
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionArgumentsAreDifferent() {
        IPerson person = new Person("Pesho");
        wimRepository.addPerson("Pesho", person);
        commandToTest.execute(parameters);
    }

    /*@Test
    public void execute_when_inputIsValid() {

        // Arrange
        IPerson person = wimFactory.createPerson(parameters.get(0));
        wimRepository.addPerson("Pesho", person);
        String personName = person.getPersonName();

        ITeam team = wimFactory.createTeam(parameters.get(1));
        wimRepository.addTeam("The Saints", team);
        team.addPersonToTeam(person);

        IBoard board = wimFactory.createBoard(parameters.get(2));
        wimRepository.addBoard("board01", board);

        IWorkItem workItem = new BugImpl("bugNameeee", "bugdescription");
        wimRepository.addWorkItem(0, workItem);
        String id = String.valueOf(workItem.getId());
        String inst = "instructions";
        String vl = "value";


        // Act
        commandToTest.execute(parameters);

        Assert.assertEquals(1, wimRepository.getTeams().size());
    }*/

}
