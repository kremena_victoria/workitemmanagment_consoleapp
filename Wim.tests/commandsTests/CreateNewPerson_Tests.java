package commandsTests;

import com.Wim.commands.CreateNewPerson;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IPerson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateNewPerson_Tests {

    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new CreateNewPerson(wimRepository, wimFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        // Act
        testCommand.execute(testList);

        // Assert
        IPerson person = wimRepository.getPersons().get("personName");
        Assert.assertEquals("personName", person.getPersonName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("personName");
        testList.add("jshfsdfhsd");

        // Act
        testCommand.execute(testList);

        // Assert
        IPerson person = wimRepository.getPersons().get("personName");
        Assert.assertEquals("personName", person.getPersonName());
    }

    @Test
    public void execute_should_createNewPerson_whenPassedValidArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("personName");

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals(1, wimRepository.getPersons().size());
    }

}
