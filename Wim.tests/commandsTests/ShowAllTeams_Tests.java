package commandsTests;

import com.Wim.commands.ShowAllTeamMembers;
import com.Wim.commands.ShowAllTeams;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShowAllTeams_Tests {

    private Command testCommand;
    private ITeam testTeam01;
    private ITeam testTeam02;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ShowAllTeams(wimRepository, wimFactory);
        testTeam01 = new Team("The Saints");
        testTeam02 = new Team("The dragons");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passed_Valid_Arguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam01.getTeamName());
        testList.add(testTeam02.getTeamName());

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkForValidArguments() {
        // Arrange
        List<String> testList = Stream.of("The Saints", "The dragons").collect(Collectors.toList());

        ITeam team01 = wimFactory.createTeam("The Saints");
        ITeam team02 = wimFactory.createTeam("The dragons");
        wimRepository.addTeam("The Saints", team01);
        wimRepository.addTeam("The dragons", team02);

        // Act & Assert
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(2, wimRepository.getTeams().size());
    }

    @Test
    public void execute_should_throwException_when_NoTeamExists() {
        List<String> testList = Stream.of("No Team created").collect(Collectors.toList());
        // Act & Assert
        Assert.assertTrue(wimRepository.getTeams().isEmpty());
    }

}
