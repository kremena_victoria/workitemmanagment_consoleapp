package commandsTests;

import com.Wim.commands.CreateNewBoardInTeam;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CreateNewBoardInTeam_Tests {

    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private List<String> parameters;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new CreateNewBoardInTeam(wimRepository, wimFactory);
        parameters = Stream.of("PersonName","TeamName","BoardName").collect(Collectors.toList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        parameters.add("Pesho");
        parameters.add("The Saints");
        // Act
        testCommand.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        // Arrange
        parameters.add("Pesho");
        parameters.add("The Saints");
        parameters.add("board01");
        parameters.add("0");
        parameters.add("NEW");
        parameters.add("dfgdfsgd");
        // Act
        testCommand.execute(parameters);
    }

    @Test
    public void execute_when_inputIsValid() {
        // Arrange

        IPerson person = new Person("Pesho");
        wimRepository.addPerson("Pesho", person);
        String personName = person.getPersonName();

        ITeam team = new Team("The Saints");
        wimRepository.addTeam("The Saints", team);
        String teamName = team.getTeamName();
        team.addPersonToTeam(person);
        String boardname = "Board 01";
        parameters = new ArrayList<>();
        parameters.add(personName);
        parameters.add(teamName);
        parameters.add(boardname);

        testCommand.execute(parameters);
    }
}
