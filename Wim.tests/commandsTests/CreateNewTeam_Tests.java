package commandsTests;

import com.Wim.commands.CreateNewTeam;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CreateNewTeam_Tests {

    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new CreateNewTeam(wimRepository, wimFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passed_LessArguments_for_Team() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("teamdName");
        // Act
        testCommand.execute(testList);

        // Assert
        ITeam team = wimRepository.getTeams().get("teamName");
        Assert.assertEquals("teamName", team.getTeamName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passed_MoreArguments_for_Team() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("teamdName");
        testList.add("personCreatedTeam");
        testList.add("ferf");

        // Act
        testCommand.execute(testList);

    }

        @Test
    public void execute_when_passed_Valid_Arguments() {

            List<String> testList = Stream.of("Pesho", "Team01").collect(Collectors.toList());

            IPerson person01 = wimFactory.createPerson(testList.get(1));
            wimRepository.addPerson("Pesho", person01);



        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals(1, wimRepository.getTeams().size());

    }
}
