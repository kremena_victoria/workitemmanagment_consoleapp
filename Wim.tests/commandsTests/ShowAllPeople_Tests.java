package commandsTests;

import com.Wim.commands.ShowAllPeople;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.membersImpl.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllPeople_Tests {

    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ShowAllPeople(wimRepository, wimFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        IPerson person = new Person("Misho");
        wimRepository.addPerson("Misho", person);
        String personName = person.getPersonName();
        Assert.assertTrue(testList.add(null));

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedValidArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        IPerson person = new Person("Misho");
        wimRepository.addPerson("Misho", person);
        String personName = person.getPersonName();
        testList.add(personName);

        // Act & Assert
        testCommand.execute(testList);
    }
}
