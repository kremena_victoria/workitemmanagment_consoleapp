package commandsTests;

import com.Wim.commands.CreateNewBoardInTeam;
import com.Wim.commands.ShowAllTeamMembers;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShowAllTeamBoards_Tests {
    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private List<String> parameters;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ShowAllTeamMembers(wimRepository, wimFactory);
        parameters = Stream.of("TeamName").collect(Collectors.toList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        parameters = new ArrayList<>();
        // Act
        testCommand.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        // Arrange
        parameters.add("Pesho");
        parameters.add("The Saints");
        parameters.add("board01");
        parameters.add("0");
        parameters.add("NEW");
        parameters.add("dfgdfsgd");
        // Act
        testCommand.execute(parameters);
    }

    @Test
    public void execute_when_inputIsValid() {
        ITeam team = new Team("The Saints");
        wimRepository.addTeam("The Saints", team);
        String teamName = team.getTeamName();

        IBoard board01 = new Board("proba1");
        wimRepository.addBoard("proba1",board01);
        IBoard board02 = new Board("proba2");
        wimRepository.addBoard("proba2",board02);
        team.addBoardToTeam(board01);
        team.addBoardToTeam(board02);

        parameters = new ArrayList<>();
        parameters.add(teamName);
        testCommand.execute(parameters);
    }

}
