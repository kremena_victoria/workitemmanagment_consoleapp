package commandsTests;

import com.Wim.commands.AssignWorkItemToAPerson;
import com.Wim.commands.UnAssignWorkItemFromAPerson;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UnAssignWorkItemFromAPerson_Tests {

    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private UnAssignWorkItemFromAPerson commandToTest;
    private List<String> parameters;
    private String testComment;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testComment = "Test comment";
        commandToTest = new UnAssignWorkItemFromAPerson(wimRepository, wimFactory);
        parameters = Stream.of("The Saints", "board01", "0", "Misho").collect(Collectors.toList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        parameters.add("The Saints");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        // Arrange
        parameters.add("The Saints");
        parameters.add("board01");
        parameters.add("0");
        parameters.add("Misho");
        parameters.add("fdsferg");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfTeamDoesNotExist() {
        parameters = new ArrayList<>();
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionArgumentsAreDifferent() {
        IPerson person = new Person("Misho");
        wimRepository.addPerson("Misho", person);
        commandToTest.execute(parameters);
    }
    @Test
    public void execute_should_addComments_when_inputIsValid() {
        IBoard board01 = new Board("immoBoard");
        wimRepository.addBoard("immoBoard", board01);
        String boardName = board01.getBoardName();
        ITeam team01 = new Team("The dragons");
        wimRepository.addTeam("The dragons", team01);
        String teamName = team01.getTeamName();
        team01.addBoardToTeam(board01);
        IPerson person = new Person("Misho");
        wimRepository.addPerson("Misho", person);
        String personName = person.getPersonName();
        team01.addPersonToTeam(person);

        IWorkItem workItem01 = new BugImpl("Bug title here", "bugDescription");
        wimRepository.getBoards().get(boardName).addWorkItemToBoard(workItem01);
        IWorkItem workItem02 = new BugImpl("Bug title here1", "bugDescription1");
        wimRepository.getBoards().get(boardName).addWorkItemToBoard(workItem02);
        board01.addBugStoryToBoard(workItem01);
        person.assignWorkItemToPerson(workItem01);
        person.getWorkItems().add(workItem01);
        person.assignWorkItemToPerson(workItem02);
        String id = String.valueOf(0);

        parameters = new ArrayList<>();
        parameters.add(teamName);
        parameters.add(boardName);
        parameters.add(id);
        parameters.add(personName);
        commandToTest.execute(parameters);
    }
}
