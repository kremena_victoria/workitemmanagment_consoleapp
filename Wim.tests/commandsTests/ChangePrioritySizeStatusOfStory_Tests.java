package commandsTests;

import com.Wim.commands.ChangePrioritySeverityStatusOfBug;
import com.Wim.commands.ChangePrioritySizeStatusOfStory;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.StoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChangePrioritySizeStatusOfStory_Tests {

    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private ChangePrioritySizeStatusOfStory commandToTest;
    private List<String> parameters;
    private String testComment;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testComment = "Test comment";
        commandToTest = new ChangePrioritySizeStatusOfStory(wimRepository, wimFactory);
        parameters = Stream.of("Pesho", "The Saints", "board01", String.valueOf(0), "instructions", "value").collect(Collectors.toList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        parameters.add("Pesho");
        parameters.add("The Saints");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        // Arrange
        parameters.add("Pesho");
        parameters.add("The Saints");
        parameters.add("board01");
        parameters.add("0");
        parameters.add("instructions");
        parameters.add("value");
        parameters.add("dfgdfsgd");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfTeamDoesNotExist() {
        parameters = new ArrayList<>();
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionArgumentsAreDifferent01() {
        ITeam team = new Team("The Saints");
        wimRepository.addTeam("The Saints", team);
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionArgumentsAreDifferent02() {
        IBoard board = new Board("board01");
        wimRepository.addBoard("board01", board);
        commandToTest.execute(parameters);
    }
    @Test
    public void execute_should_addComments_when_inputIsValid() {
        // Arrange
        IPerson person = new Person("personWhoAdded");
        wimRepository.addPerson("personWhoAdded", person);
        String personName = person.getPersonName();

        ITeam team01 = new Team("The dragons");
        wimRepository.addTeam("The dragons", team01);
        String teamName = team01.getTeamName();

        IBoard board01 = new Board("immoBoard");
        wimRepository.addBoard("immoBoard", board01);
        String boardName = board01.getBoardName();

        team01.addPersonToTeam(person);
        team01.addBoardToTeam(board01);

        IWorkItem workItem01 = new StoryImpl("Bug title here", "bugDescription");
        wimRepository.getBoards().get(boardName).addWorkItemToBoard(workItem01);
        String id = String.valueOf(0);

        parameters = new ArrayList<>();
        parameters.add(personName);
        parameters.add(teamName);
        parameters.add(boardName);
        parameters.add(id);
        parameters.add("priority");
        parameters.add("high");

        // Act
        commandToTest.execute(parameters);
    }




}
