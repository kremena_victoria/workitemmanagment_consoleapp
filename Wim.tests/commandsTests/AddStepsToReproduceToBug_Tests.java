package commandsTests;

import com.Wim.commands.AddPersonToTeam;
import com.Wim.commands.AddStepsToReproduceToBug;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemContracts.IBug;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddStepsToReproduceToBug_Tests {
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private AddStepsToReproduceToBug commandToTest;
    private List<String> parameters;
    private String testStr;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        commandToTest = new AddStepsToReproduceToBug(wimRepository, wimFactory);
        testStr = "steps here";
        parameters = Stream.of("personToAddBugSteps", "teamToAddBugSteps", "boardNameToAddStepsToReproduce",String.valueOf(0), "stepToReproduceToBeAdded").collect(Collectors.toList());

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        parameters = new ArrayList<>();
        // Arrange
        parameters.add("person01");
        parameters.add("team01");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        // Arrange
        parameters.add("person01");
        parameters.add("team01");
        parameters.add("board01");
        parameters.add("0");
        parameters.add("steps many steps");
        parameters.add("dsgfd");
        // Act
        commandToTest.execute(parameters);
    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_Exception_if_person_not_created() {
        IPerson person = new Person("Pesho");
        wimRepository.addPerson("Pesho", person);
        commandToTest.execute(parameters);
    }

    @Test
    public void execute_should_addComments_when_inputIsValid() {
        // Arrange
        IPerson person = new Person("personWhoAdded");
        wimRepository.addPerson("personWhoAdded", person);
        String personName = person.getPersonName();

        ITeam team01 = new Team("The dragons");
        wimRepository.addTeam("The dragons", team01);
        String teamName = team01.getTeamName();

        IBoard board01 = new Board("immoBoard");
        wimRepository.addBoard("immoBoard", board01);
        String boardName = board01.getBoardName();

        team01.addPersonToTeam(person);

        IWorkItem workItem01 = new BugImpl("Bug title here", "bugDescription");
        wimRepository.getBoards().get(boardName).addWorkItemToBoard(workItem01);
        String id = String.valueOf(0);

        parameters = new ArrayList<>();
        parameters.add(personName);
        parameters.add(teamName);
        parameters.add(boardName);
        parameters.add(id);
        parameters.add(testStr);

        // Act
        commandToTest.execute(parameters);
    }

}
