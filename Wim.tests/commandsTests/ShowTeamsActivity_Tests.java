package commandsTests;

import com.Wim.commands.ShowBoardsActivity;
import com.Wim.commands.ShowTeamsActivity;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShowTeamsActivity_Tests {

    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private ITeam testTeam;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ShowTeamsActivity(wimRepository, wimFactory);
        testTeam = new Team("The Saints");
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfTeamDoesNotExist() {
        List<String> testList = new ArrayList<>();
        testCommand.execute(testList);
    }

    @Test
    public void execute_when_passed_Valid_Arguments() {
        // Arrange
        List<String> testList = Stream.of("The Saints").collect(Collectors.toList());

        ITeam team = wimFactory.createTeam(testList.get(0));
        ITeam team1 = wimFactory.createTeam("The Perfect Team");

        wimRepository.addTeam("The Saints", team);

        // Act
        testCommand.execute(testList);

        // Arrange
        Assert.assertEquals(1, wimRepository.getTeams().size());
    }

}
