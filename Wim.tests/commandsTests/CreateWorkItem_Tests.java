package commandsTests;
import com.Wim.commands.CreateWorkItem;
import com.Wim.commands.contracts.Command;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemContracts.IBug;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.WorkItemBase;
import modelsTests.WorkItemBase_Tests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CreateWorkItem_Tests {
    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private IWorkItem testWorkItem;
    private List<String> parameters;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new CreateWorkItem(wimRepository, wimFactory);
        testWorkItem = new BugImpl("workItemTitle", "WorkItemDescription");
        parameters = Stream.of(
                "nameOfTeam", "nameOfBoard", "Bug", "This is TestItem", "This is description")
                .collect(Collectors.toList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionForInvalidNumberOfArguments() {
        testCommand.execute(new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfTeamDoesNotExist() {
        testCommand.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfBoardDoesNotExist() {
        wimRepository.addTeam("nameOfTeam", new Team("teamName"));
        testCommand.execute(parameters);
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_failedToParseParameters() {
        List arguments = Stream.of(5, 5,5,5,5).collect(Collectors.toList());

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        // Act
        testCommand.execute(testList);

        // Assert
        IWorkItem workItem = wimRepository.getWorkItems().get("workItemTitle");
        Assert.assertEquals("workItemTitle", workItem.getTitle());
    }

    @Test
    public void execute_when_inputIsValid() {
        // Arrange
        List<String> testList = new ArrayList<>();

        IPerson person = new Person("Pesho");
        wimRepository.addPerson("Pesho", person);
        String personName = person.getPersonName();

        ITeam team = new Team("The Saints");
        wimRepository.addTeam("The Saints", team);
        String teamName = team.getTeamName();
        team.addPersonToTeam(person);

        IBoard board = new Board("board01");
        wimRepository.addBoard("board01", board);
        String boardname = board.getBoardName();
        team.addBoardToTeam(board);

        IWorkItem workItem = new BugImpl("bug name here", "bug description here");
        wimRepository.addWorkItem(0, workItem);
        String workItemName = workItem.getTitle();
        String workItemDescription = workItem.getDescription();
        String workItemtype = workItem.getType();

        testList.add(personName);
        testList.add(teamName);
        testList.add(boardname);
        testList.add(workItemName);
        testList.add(workItemDescription);
        testList.add(workItemtype);
        // Act
        testCommand.execute(testList);
    }

}
