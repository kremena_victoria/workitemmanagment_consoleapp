package commandsTests;

import com.Wim.commands.AddCommentsToAWorkItem;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import com.Wim.models.contracts.IPerson;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.workItemContracts.Comment;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.CommentImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddCommentsToAWorkItem_Tests {

    private WimRepository wimRepository;
    private WimFactory wimFactory;
    private AddCommentsToAWorkItem commandToTest;
    private List<String> parameters;
    private String testComment;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testComment = "Test comment";
        commandToTest = new AddCommentsToAWorkItem(wimRepository, wimFactory);
        parameters = Stream.of("Peshoo",String.valueOf(0), "NqkakuvKomentar").collect(Collectors.toList());
    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        parameters.add("njakoj");
        parameters.add("1");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        // Arrange
        parameters.add("njakoj");
        parameters.add("1");
        parameters.add("njakakav komentar");
        parameters.add("dsfdf");
        // Act
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionIfTeamDoesNotExist() {
        parameters = new ArrayList<>();
        commandToTest.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_Should_Throw_ExceptionArgumentsAreDifferent() {
        IPerson person = new Person("Pesho");
        wimRepository.addPerson("Pesho", person);
        commandToTest.execute(parameters);
    }

    @Test
    public void execute_should_addComments_when_inputIsValid() {
        // Arrange
        IPerson person = new Person("Pesho");
        wimRepository.addPerson("Pesho", person);
        String personName = person.getPersonName();
        IWorkItem workItem01 = new BugImpl("Bug title here", "jfdsfffsfdfsfdsd");
        wimRepository.addWorkItem(workItem01.getId(), workItem01);
        int testId = workItem01.getId();
        parameters = new ArrayList<>();
        parameters.add(personName);
        parameters.add(String.valueOf(testId));
        parameters.add("Our comment here");

        // Act
        commandToTest.execute(parameters);
    }
}
