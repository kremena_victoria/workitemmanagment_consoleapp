package coreTests;

import com.Wim.core.contracts.CommandParser;
import com.Wim.core.providers.CommandParserImpl;;
import com.Wim.models.workItemImpl.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CommandParserImpl_Tests {
    private String fullCommand = "createnewperson Misho";
    private CommandParser comm;

    @Before
    public void before() {
        comm = new CommandParserImpl();
    }

    @Test
    public void should_return_fullCommand() {
        String command = "";
        Assert.assertEquals("", comm.parseCommand(command));
    }
    @Test
    public void should_return_command_parameters() {
        Assert.assertEquals("Misho", comm.parseParameters(fullCommand).toString()
                .replaceAll("\\[","")
                .replaceAll("\\]",""));
    }
    @Test
    public void should_return_empty_parameter() {
        fullCommand = "";
        comm.parseParameters(fullCommand);
        Assert.assertEquals("", comm.parseCommand(fullCommand));
    }
}
