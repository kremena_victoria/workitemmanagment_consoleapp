package coreTests;

import com.Wim.commands.*;
import com.Wim.commands.contracts.CommandType;
import com.Wim.core.contracts.CommandWim;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.contracts.WimRepository;
import com.Wim.core.factories.CommandFactoryImpl;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.core.factories.WimRepositoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CommandFactoryImpl_Tests {
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    CommandType commandType;
    CommandWim testCommand;

    @Before
        public void before() {
            wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new CommandFactoryImpl();
    }
    @Test(expected = IllegalArgumentException.class)
    public void should_throwError_when_CommandIs_incorrect() {
        CommandType commandType = CommandType.NOTDECLARED;
       testCommand.createCommand(String.valueOf(commandType),wimFactory,wimRepository);


    }
    @Test(expected = IllegalArgumentException.class)
    public void should_throwError_when_CommandIs_Not_from_the_Constant_List() {
        String commandTypeAsString = "show";
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());
    }
    @Test
    public void should_createNewPerson_when_pass() {
        String commandTypeAsString = "CreateNewPerson";
        Assert.assertNotSame(new CreateNewPerson(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_createNewTeam_when_pass() {
        String commandTypeAsString = "CreateNewTeam";
        Assert.assertNotSame(new CreateNewTeam(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }

    @Test
    public void should_addPersonToTeam_when_pass() {
        String commandTypeAsString = "AddPersonToTeam";
        Assert.assertNotSame(new AddPersonToTeam(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_showAllPeople_when_pass() {
        String commandTypeAsString = "ShowAllPeople";
        Assert.assertNotSame(new ShowAllPeople(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ShowAllTeams_when_pass() {
        String commandTypeAsString = "ShowAllTeams";
        Assert.assertNotSame(new ShowAllTeams(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_CreateNewBoardInTeam_when_pass() {
        String commandTypeAsString = "CreateNewBoardInTeam";
        Assert.assertNotSame(new CreateNewBoardInTeam(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ShowAllTeamBoards_when_pass() {
        String commandTypeAsString = "ShowAllTeamBoards";
        Assert.assertNotSame(new ShowAllTeamBoards(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ShowAllTeamMembers_when_pass() {
        String commandTypeAsString = "ShowAllTeamMembers";
        Assert.assertNotSame(new ShowAllTeamMembers(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_CreateWorkItem_when_pass() {
        String commandTypeAsString = "CreateAWorkItem";
        Assert.assertNotSame(new CreateWorkItem(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_AddStepsToReproduceToBug_when_pass() {
        String commandTypeAsString = "AddStepsToReproduceToBug";
        Assert.assertNotSame(new AddStepsToReproduceToBug(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ShowPersonActivity_when_pass() {
        String commandTypeAsString = "ShowPersonActivity";
        Assert.assertNotSame(new ShowPersonActivity(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ChangeRatingToFeedback_when_pass() {
        String commandTypeAsString = "ChangeRatingToFeedback";
        Assert.assertNotSame(new ChangeRatingToFeedback(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ShowBoardsActivity_when_pass() {
        String commandTypeAsString = "ShowBoardsActivity";
        Assert.assertNotSame(new ShowBoardsActivity(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ShowTeamsActivity_when_pass() {
        String commandTypeAsString = "ShowTeamsActivity";
        Assert.assertNotSame(new ShowTeamsActivity(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_AssignWorkItemToAPerson_when_pass() {
        String commandTypeAsString = "AssignWorkItemToAPerson";
        Assert.assertNotSame(new AssignWorkItemToAPerson(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_UnAssignWorkItemFromAPerson_when_pass() {
        String commandTypeAsString = "UnAssignWorkItemFromAPerson";
        Assert.assertNotSame(new UnAssignWorkItemFromAPerson(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ChangeStatusOfFeedback_when_pass() {
        String commandTypeAsString = "ChangeStatusOfFeedback";
        Assert.assertNotSame(new ChangeStatusOfFeedback(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_AddCommentsToAWorkItem_when_pass() {
        String commandTypeAsString = "AddCommentToAWorkItem";
        Assert.assertNotSame(new AddCommentsToAWorkItem(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ChangePrioritySeverityStatusOfBug_when_pass() {
        String commandTypeAsString = "ChangePrioritySeverityStatusOfBug";
        Assert.assertNotSame(new ChangePrioritySeverityStatusOfBug(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ChangePrioritySizeStatusOfStory_when_pass() {
        String commandTypeAsString = "ChangePrioritySizeStatusOfStory";
        Assert.assertNotSame(new ChangePrioritySizeStatusOfStory(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
    @Test
    public void should_ListWorkItems_when_pass() {
        String commandTypeAsString = "ListWorkItems";
        Assert.assertNotSame(new ListWorkItems(wimRepository,wimFactory),testCommand.createCommand(commandTypeAsString,wimFactory,wimRepository));
    }
}
