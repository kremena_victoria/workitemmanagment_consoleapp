package coreTests;

import com.Wim.commands.contracts.CommandType;
import com.Wim.core.contracts.WimFactory;
import com.Wim.core.factories.WimFactoryImpl;
import com.Wim.models.contracts.IBoard;
import com.Wim.models.contracts.ITeam;
import com.Wim.models.membersImpl.Board;
import com.Wim.models.membersImpl.Person;
import com.Wim.models.membersImpl.Team;
import com.Wim.models.workItemContracts.IWorkItem;
import com.Wim.models.workItemImpl.BugImpl;
import com.Wim.models.workItemImpl.FeedbackImpl;
import com.Wim.models.workItemImpl.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WimFactoryImpl_Tests {
    private WimFactory factory;
    @Before
    public void before() {
        factory = new WimFactoryImpl();
    }
    @Test
    public void createWorkItem_should_createNewWorkItem_when_withValidWorkItemType01() {
        // Arrange & Act
        IWorkItem workItem = factory.createWorkItem("bug", "workItem title", "workItem descr");

        // Assert
        Assert.assertEquals("Bug", workItem.getType());
    }

    @Test
    public void createWorkItem_should_createNewWorkItem_when_withValidWorkItemType02() {
        // Arrange & Act
        IWorkItem workItem = factory.createWorkItem("story", "workItem title", "workItem descr");

        // Assert
        Assert.assertEquals("Story", workItem.getType());
    }

    @Test
    public void createWorkItem_should_createNewWorkItem_when_withValidWorkItemType03() {
        // Arrange & Act
        IWorkItem workItem = factory.createWorkItem("feedback", "workItem title", "workItem descr");

        // Assert
        Assert.assertEquals("Feedback", workItem.getType());
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void constructor_should_throwError_when_personNameIsTooShort() {
//        Person person01 = new Person("kim");
//    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void constructor_should_throwError_when_personNameIsTooLong() {
//        Person person01 = new Person("PIPILOTA VICTORIA ESPERANZA DALGOTO CHORAPCHE");
//    }

    @Test
    public void constructor_should_returnPerson_when_valueIsValid() {
        Person person01 = new Person("Kimberly");
        Assert.assertEquals(person01.getPersonName(), "Kimberly");

    }
    @Test
    public void should_createTeam_when_withValid() {
        ITeam team = factory.createTeam("The saints");
        Assert.assertEquals("The saints",team.getTeamName());
    }
    @Test
    public void should_createBoard_when_withValid() {
        IBoard board = factory.createBoard("The saints");
        Assert.assertEquals("The saints", board.getBoardName());
    }
    @Test(expected = IllegalArgumentException.class)
    public void should_throwError_when_workItemType_incorrect() {
        IWorkItem workItem = factory.createWorkItem("incorrect", "workItem title", "workItem descr");
    }
}
